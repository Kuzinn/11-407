package ru.kpfu.itis.excel;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.util.IntegerField;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;
import ru.kpfu.itis.api.domain.Equipment;
import ru.kpfu.itis.api.service.CarService;
import ru.kpfu.itis.api.service.EquipmentService;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kuzin on 05.04.2016.
 */
public class ExcelParser {


    public List<List<String>> parse(File file) throws IOException, InvalidFormatException {//MultipartFile mpf
        Workbook workbook = new XSSFWorkbook(file);//mpf.getInputStream()
        Sheet sheet = workbook.getSheetAt(2);//TODO исправить

        /*
        Валидации файлов:
               файл с комплектациями:
                    порядок опций должен быть именно тем что указан в шапке таблицы
               файл с автомобилями:
                    порядок характеристик авто должен быть именно тем что укзан в шапке таблицы
                    на втором листе файла должна находиться базовая комплектация авто
         */


        //если имя файла содержит слово 'equipment' тогда мы подразумеваем что это комплектация и добавляем ее к машине

        //EQUIPMENT PARSING
        List<List<String>> map = new ArrayList<List<String>>();
        for(int j=1;j<=sheet.getLastRowNum();j++){
            Row r=sheet.getRow(j);
            List<String> list=new ArrayList<String>();
            for (int i = 0; i <r.getLastCellNum() ; i++) {
                System.out.print(r.getCell(i).getCellType()+"   ");
                if(r.getCell(i).getCellType()==1){
                    String s="'";
                    s+=r.getCell(i).getStringCellValue();
                    s+="'";
                    if(s.equals("'+'")){
                        s="true";
                    }else if(s.equals("'-'")){
                        s="false";
                    }
                    list.add(s);
                }else{
                    Double d=r.getCell(i).getNumericCellValue();
                    list.add(d.toString());
                }
            }

            map.add(list);
        }

        //иначе если имя файла содержит подстроку 'car', то добавляем авто









        return map;
    }


//    public void insertParsedData(List<List<String>> parsedData){
//        try {
//
//            for (int i = 0; i <parsedData.size() ; i++) {
//                equipmentService.addEquipmentByValue(parsedData.get(i));
//            }
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InvalidFormatException e) {
//            e.printStackTrace();
//        }
//    }


    public static void main(String[] args) {
        File f=new File("C:/Users/kuzin/Desktop/test.xlsx");
        ExcelParser excelParser=new ExcelParser();
        try {
            excelParser.parse(f);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
    }

}
