package ru.kpfu.itis.controllers;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.service.CarService;
import ru.kpfu.itis.api.service.EquipmentService;
import ru.kpfu.itis.api.service.ReviewService;
import ru.kpfu.itis.excel.ExcelParser;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * Created by kuzin on 14.04.2016.
 */

@Controller
@RequestMapping(value = "/parse")
public class ParseController {
    @Autowired
    private CarService carService;

    @Autowired
    private EquipmentService equipmentService;

    @RequestMapping(value = "/xml",method = RequestMethod.GET)
    public String parseXML(){
        File f=new File("C:/Users/kuzin/Desktop/test.xlsx");
        ExcelParser excelParser=new ExcelParser();



        return "parsexml";
    }

    @RequestMapping(value = "/autoru",method = RequestMethod.GET)
    public String parseAUTORU(){
        return "parseautoru";
    }


}
