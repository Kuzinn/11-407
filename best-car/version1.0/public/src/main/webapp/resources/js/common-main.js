$(document).ready(function(){
	 $(".srch-box-vendr-loadmore-lnk").on("click",function(){
  	$(".srch-box-vendr").addClass("expanded");
  });
 
  $("._money").autoNumeric('init', {
  	aSep: ' ', 
  	aDec: '.', 
  	vMin: '0.00',
  	aSign: '₽ '
  });
  $(".owl-carousel").owlCarousel({
  	items : 4,
  	margin : 30,
  	nav : true,
  	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
  });

	function collectFormData(fields) {
		var data = {};
		for (var i = 0; i < fields.length; i++) {
			var $item = $(fields[i]);
			data[$item.attr('name')] = $item.val();
		}
		return data;
	}
	var $form = $('#registration');
	$form.bind('submit', function(e) {
		// Ajax validation
		var $inputs = $form.find('input');
		var data = collectFormData($inputs);

		$.post('/reg.json', data, function(response) {
			$form.find('.control-group').removeClass('error');
			$form.find('.help-inline').empty();
			$form.find('.alert').remove();

			if (response.status == 'FAIL') {
				for (var i = 0; i < response.errorMessageList.length; i++) {
					var item = response.errorMessageList[i];
					$("#help-inline").prepend("<div>"+item.defaultMessage+"</div>");
				}
			} else {
				document.location.href = '/checkmail';

			}
		}, 'json');

		e.preventDefault();
		return false;
	});

});
//parallax
$(window).scroll(function(){
  var st = $(this).scrollTop();

  $(".head").css({
    "transform" : "translate(0%, " + st /50 + "%"
  });
});

//checkbox styles
$('[type=checkbox]').checkbox({checkboxClass:'srch-box-checkbox', labelClass:'srch-box-chboxlabel'});
