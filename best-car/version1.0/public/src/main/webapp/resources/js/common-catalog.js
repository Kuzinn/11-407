$(document).ready(function(){

	 $(".srch-box-vendr-loadmore-lnk").on("click",function(){
  	$(".srch-box-vendr").addClass("expanded");
  });
  $("._money").autoNumeric('init', {
  	aSep: ' ', 
  	aDec: '.', 
  	vMin: '0.00',
  	aSign: '₽ '
  });  
  var headers = ["H1","H2","H3","H4","H5","H6"];

$(".accordion").click(function(e) {
  var target = e.target,
      name = target.nodeName.toUpperCase();
  
  if($.inArray(name,headers) > -1) {
    var subItem = $(target).next();
    
    //slideUp all elements (except target) at current depth or greater
    var depth = $(subItem).parents().length;
    var allAtDepth = $(".accordion p, .accordion div").filter(function() {
      if($(this).parents().length >= depth && this !== subItem.get(0)) {
        return true; 
      }
    });
    $(allAtDepth).slideUp("fast");
   

    //slideToggle target content and adjust bottom border if necessary
    subItem.slideToggle("fast",function() {
        $(".accordion :visible:last").css("border-radius","0 0 10px 10px");
    });
    
  }
});
$(".js_customHeader").click(function(){
	$(".js_customHeaderDiv").slideToggle("fast");
	$(".js_customHeaderDiv").css("background-color","#B1B0B0")
});
    $('[type=checkbox]').checkbox({checkboxClass:'srch-box-checkbox', labelClass:'srch-box-chboxlabel'});

});