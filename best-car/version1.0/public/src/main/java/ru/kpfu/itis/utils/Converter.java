package ru.kpfu.itis.utils;

import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.searching.FiltersTree;
import ru.kpfu.itis.searching.Node;
import ru.kpfu.itis.searching.Variant;
import ru.kpfu.itis.searching.VariantType;
//import sun.invoke.util.VerifyAccess;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.FileHandler;

/**
 * Created by kuzin on 27.03.2016.
 */
public class Converter {
    public static String trueFalseToString(String trueS,String falseS){
        if(trueS!=null){
            return "true";
        }
        if(falseS!=null){
            return "false";
        }
        return null;
    }

    public static Double getAvgRatingByCarID(){
        List<Integer> reviews=new ArrayList<Integer>();
        Double ratingSumm=0.0;
        for(Integer r: reviews){
            ratingSumm+=Double.valueOf(r);
        }

        Double rating = ratingSumm / reviews.size();
        if (rating%1 <= 0.2){
            rating = Math.floor(rating);
        }else if(rating%1 >= 0.8) {
            rating = Math.ceil(rating);
        }
        return rating;
    }

    public static void main(String[] args) {
        String s="P 2 500 000.00";
        System.out.println(s.replace(" ","").substring(1).replace(".00",""));
        String hello="Hello kitty!";
        System.out.println(hello.replace(" ",""));
        System.out.println(hello.trim());
    }

//    public static void main(String[] args) {
//        FiltersTree ft=new FiltersTree();
//        Node n=new Node("Цена");
//        Variant variant=new Variant("equipment","price",VariantType.TEXT);
//        variant.addValue("15000");
//        variant.addValue("999990");
//        n.addVariant(variant);
//        ft.addRootChild(n);
//
//        Node n1=new Node("Безопасность");
//        Variant variant1=new Variant("equipment","airbag",VariantType.RADIO);
//        variant1.addValue("'есть'");
//        n1.addVariant(variant1);
//        ft.addRootChild(n1);
//
//        Node n2=new Node("Основные характеристики");
//        Node n2_1=new Node("Двигатель");
//        Variant variant2=new Variant("equipment","engineType",VariantType.CHECKBOX);
//        variant2.addValue("бензиновый");
//        Variant variant3=new Variant("equipment","engineType", VariantType.CHECKBOX);
//        variant3.addValue("дизельный");
//        n2_1.addVariant(variant2);
//        n2_1.addVariant(variant3);
//        Node n2_2=new Node("Состояние");
//        Variant variant4=new Variant("equipment","condition",VariantType.CHECKBOX);
//        variant4.addValue("новая");
//        Variant variant5=new Variant("equipment","condition",VariantType.CHECKBOX);
//        variant5.setNode(true);
//        Node n2_2_1=new Node("Пробег");
//        Variant variant6=new Variant("equipment","mileage",VariantType.TEXT);
//        variant6.addValue("10000");
//        variant6.addValue("100000");
//        n2_2_1.addVariant(variant6);
//        variant5.addChild(n2_2_1);
//        n2_2.addVariant(variant4);
//        n2_2.addVariant(variant5);
//        n2.addChild(n2_1);
//        n2.addChild(n2_2);
//        ft.addRootChild(n2);
//
//
//
//        System.out.println(ft.toSQLQuery());
//    }
}
