package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class CatalogController extends BaseController {
    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private FavouritesService favouritesService;

    @Autowired
    private CompareService compareService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ReviewService reviewService;

    @CompareCountInit
    @RequestMapping(value = "/catalog", method = RequestMethod.GET)
    public String renderCatalogPage(){
        return "catalog/catalog2";
    }

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/main-filter",method = RequestMethod.POST)
    public String mainFilter(Model model) {

        String mf = request.getParameter("money_from");
        String mt = request.getParameter("money_to");
        model.addAttribute("moneyFrom",mf);
        model.addAttribute("moneyTo",mt);
        String fromStore = request.getParameter("new");
        String used = request.getParameter("used");
        String manual = request.getParameter("Ручная");
        String automatic = request.getParameter("Автомат");
        Map<String,String[]> paramsMap = request.getParameterMap();
        mf= mf.length()>2?mf.substring(2).replace(" ",""):"";
        mt= mt.length()>2?mt.substring(2).replace(" ",""):"";

        Double money_from ;
        Double money_to;


        try {
            money_from = new Double(mf);
        }catch (NumberFormatException nfe){
            money_from=0.0;
        }
        try {
            money_to = new Double(mt);
        }catch (NumberFormatException nfe){
            money_to=1000000000000.0;
        }

        List<String> conditions=new ArrayList<String>();
        List<String> transmissions=new ArrayList<String>();
        if(fromStore!=null){
            fromStore="новая";
            conditions.add(fromStore);
            model.addAttribute("fromStore","checked");
        }else {
            model.addAttribute("fromStore", "");
        }
        if(used!=null){
            used="б.у";
            conditions.add(used);
            model.addAttribute("used","checked");
        }else{
            model.addAttribute("used","");
        }
        //TODO HARDCODE!!!!!!!!!!!!!!!!!!!!
        if(manual!=null){
            manual="механическая%";
            transmissions.add(manual);
            model.addAttribute("manual","checked");
        }else{
            model.addAttribute("manual","");
        }
        if(automatic!=null){
            automatic="автомат%";
            transmissions.add(automatic);
            model.addAttribute("automatic","checked");
        }else{
            model.addAttribute("automatic","");
        }

        List<String> brands=new ArrayList<String>();
        for(String s:paramsMap.keySet()){
            System.out.println(s);
            if(s.substring(0,1).contains("_")){
                brands.add(s.substring(1));
                model.addAttribute(s,"checked");
                System.out.println(s);
            }
        }
        List<Car> cars=carService.getCarByMainFilters(money_from,money_to,conditions,transmissions,brands);

        Map<String,Double> ratingMap=new TreeMap<String, Double>();
        for(Car c: cars){
            ratingMap.put(c.getId().toString(),reviewService.getAvgRatingByCarID(c.getId()));
        }
        model.addAttribute("cars",cars);
        model.addAttribute("ratingMap",ratingMap);

        return "catalog/catalog2";
    }

    @RegistrationFormBeanInit
    @RequestMapping(value = "/main-filter",method = RequestMethod.GET)
    public String mainFilterRedirect(){
        return "redirect:/";
    }

}
