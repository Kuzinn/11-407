package ru.kpfu.itis.searching;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 05.04.2016.
 */
public class FiltersList {
    List<Variant> filters;

    public FiltersList() {
        filters = new ArrayList<Variant>();
    }

    public List<Variant> getFilters() {
        return filters;
    }

    public void setFilters(List<Variant> filters) {
        this.filters = filters;
    }

    public void add(Variant var) {
        filters.add(var);
    }

    public void update(Variant var) {
        for (Variant vars : filters) {
            if (vars.getName().equals(var.getName())) {
                filters.remove(vars);
                filters.add(var);
            }
        }
    }

    public Variant getVariantByName(String name){
        Variant variant=null;
        for (Variant vars : filters) {
            if (vars.getName().equals(name)) {
                variant=vars;
            }
        }
        return variant;
    }

    public String toSQLQuery() {
        StringBuilder sb = new StringBuilder("select * from car,equipment where car.id=equipment.carId_id ");
        for (Variant variant : filters) {
            sb.append(" and ");
            if (variant.getType() == VariantType.CHECKBOX) {
                for (String value : variant.getValues()) {
                    sb.append(" ( ");
                    sb.append(variant.getTable());
                    sb.append(".");
                    sb.append(variant.getName());
                    sb.append(" = ");
                    sb.append(value);
                    sb.append(")");
                    if (variant.getValues().indexOf(value) < variant.getValues().size() - 1) {
                        sb.append(" or ");
                    }
                }
            } else if (variant.getType() == VariantType.RADIO) {
                for (String value : variant.getValues()) {
                    sb.append(" ( ");
                    sb.append(variant.getTable());
                    sb.append(".");
                    sb.append(variant.getName());
                    sb.append(" = ");
                    sb.append(value);
                    sb.append(")");
                }
            } else {
                String from=variant.getValues().get(0);
                String to=variant.getValues().get(1);

                sb.append(" ( ");
                sb.append(variant.getTable());
                sb.append(".");
                sb.append(variant.getName());
                if(from!=null && to!=null) {

                    sb.append(" > ");
                    sb.append(from);//from
                    sb.append(" and ");
                    sb.append(variant.getTable());
                    sb.append(".");
                    sb.append(variant.getName());
                    sb.append(" < ");
                    sb.append(to);//to

                }else if(from!=null){
                    sb.append(" > ");
                    sb.append(from);//from
                }else if(to!=null){
                    sb.append(" < ");
                    sb.append(to);//to
                }else{
                    throw new NullPointerException();
                }
                sb.append(")");

            }
        }
        return sb.toString();
    }


}
