package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by kuzin on 08.03.2016.
 */
@Controller
public class BaseController {

    @Autowired
    protected HttpServletRequest request;
}
