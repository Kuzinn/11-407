package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;
import ru.kpfu.itis.form_validation.ReviewFormBean;

import javax.validation.Valid;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class ReviewController extends BaseController{
    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private ReviewService reviewService;


    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/create-review", method = RequestMethod.POST)
    public String renderCreateReviewPage(Long id,Model model){
        model.addAttribute("car",carService.getCarById(id));
        request.setAttribute("reviewForm", new ReviewFormBean());
        return "create-review/c_review";
    }

    @RequestMapping(value = "/create-review/new-review",method = RequestMethod.POST)
    public String addNewReview(@Valid @ModelAttribute("reviewForm") ReviewFormBean reviewFormBean, BindingResult bindingResult, Long id){
        if (bindingResult.hasErrors()) {
            request.setAttribute("car",carService.getCarById(id));
            return "create-review/c_review";
        }
        Review review=new Review();
        review.setCar(carService.getCarById(id));
        review.setUser(userService.getUserByName(request.getUserPrincipal().getName()));
        review.setRating(reviewFormBean.getRating());
        review.setGood(reviewFormBean.getGood());
        review.setBad(reviewFormBean.getBad());
        review.setComment(reviewFormBean.getComment());
        reviewService.addReview(review);

        return "redirect:/item?id="+id;
    }

}
