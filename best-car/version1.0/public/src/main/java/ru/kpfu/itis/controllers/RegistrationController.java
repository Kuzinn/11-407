package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.service.UserService;
import ru.kpfu.itis.form_validation.PopupFormValidationResponse;
import ru.kpfu.itis.form_validation.RegistrationFormBean;
import ru.kpfu.itis.mail.ssl.Sender;
import ru.kpfu.itis.utils.SHAEncoder;
import ru.kpfu.itis.utils.Vaildator;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class RegistrationController extends BaseController {

    @Autowired
    private UserService userService;

//    /**
//     * Если кто-то попытается зарегистрироваться не введя все данные верно и не используя веб-интерфейс , то этот метод вернет редирект на гугл
//     *
//     * @param request
//     * @return
//     */
//    @RequestMapping(value = "/register", method = RequestMethod.POST)
//    public String registerNewUser(HttpServletRequest request) {
//        String name=request.getParameter("name");
//        String email=request.getParameter("email");
//        String pass=request.getParameter("password");
//        String passTwo=request.getParameter("passwordtwo");
//        if(Vaildator.registerFormIsNorm(name,pass,passTwo,email)) {
//            userService.createNewUser(name, email, SHAEncoder.encode(pass), "ROLE_USER");
//            Sender sslSender = new Sender("mail.bestcar@gmail.com", "simplepass");
//            sslSender.send("Подтверждение регистрацей на BestCar.com", "Поздравляем с регистрации на bestcar.com. \n " +
//                    "Чтобы активировать аккаунт пожалуйста перейдите по ссылке ниже и авторизуйтесь! \n" +
//                    "http://localhost:8080/activate-user-with-id?id=" + userService.getUserByName(name).getId(), "mail.bestcar@gmail.com", email);
//        }else{
//
//            return "redirect:https://www.google.ru/?q=why+are+you+so+rude#newwindow=1&q=why+are+you+so+rude";
//        }
//        return "registration/checkmail";
//    }


    @RequestMapping(value = "/checkmail")
    public String renderCheckMailPage(){
        return "registration/checkmail";
    }




    @RequestMapping(value="/reg.json",method = RequestMethod.POST)
    public @ResponseBody
    PopupFormValidationResponse processForm (Model model, @Valid RegistrationFormBean registrationFormBean, BindingResult result ) {
        PopupFormValidationResponse res = new PopupFormValidationResponse();
        if(result.hasErrors()){
            res.setStatus("FAIL");
            res.setErrorMessageList(result.getAllErrors());
        }else{
            res.setStatus("SUCCESS");
            userService.createNewUser(registrationFormBean.getNickname(), registrationFormBean.getEmail(), SHAEncoder.encode(registrationFormBean.getPassword()), "ROLE_USER");
            Sender sslSender = new Sender("mail.bestcar@gmail.com", "simplepass");
            sslSender.send("Подтверждение регистрацей на BestCar.com", "Поздравляем с регистрации на bestcar.com. \n " +
                    "Чтобы активировать аккаунт пожалуйста перейдите по ссылке ниже и авторизуйтесь! \n" +
                    "http://localhost:8080/activate-user-with-id?id=" + userService.getUserByName(registrationFormBean.getNickname()).getId(), "mail.bestcar@gmail.com", registrationFormBean.getEmail());
        }

        return res;
    }


    @RequestMapping(value = "/activate-user-with-id",method = RequestMethod.GET)
    public String activateUser(Long id){
        User user=userService.getUserById(id);
        user.setEnabled(true);
        userService.updateUser(user);
        return "redirect:/login";
    }

}
