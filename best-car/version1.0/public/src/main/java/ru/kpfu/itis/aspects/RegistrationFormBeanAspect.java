package ru.kpfu.itis.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.kpfu.itis.form_validation.RegistrationFormBean;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by kuzin on 13.04.2016.
 */
@Aspect
@Component
public class RegistrationFormBeanAspect {

    @Autowired
    private HttpServletRequest request;

    @Pointcut("@annotation(ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit)")
    public void registrationFormBeanInitMethod() {
    }

    @Before("registrationFormBeanInitMethod()")
    public void initCompareCount() {
        request.setAttribute("registrationForm", new RegistrationFormBean());
    }
}
