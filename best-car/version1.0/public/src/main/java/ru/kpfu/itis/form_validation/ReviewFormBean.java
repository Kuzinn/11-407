package ru.kpfu.itis.form_validation;

import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

/**
 * Created by kuzin on 04.04.2016.
 */
public class ReviewFormBean {

    @NotNull(message = "Поле обязательно для заполнения")
    private Integer rating;

    @NotEmpty(message = "Поле обязательно для заполнения")
    private String good;

    @NotEmpty(message = "Поле обязательно для заполнения")
    private String bad;

    @NotEmpty(message = "Поле обязательно для заполнения")
    private String comment;


    public ReviewFormBean() {
    }

    public ReviewFormBean(int rating,String good, String bad, String email, String comment) {
        this.rating = rating;
        this.good = good;
        this.bad = bad;
        this.comment = comment;
    }

    public String getBad() {
        return bad;
    }

    public void setBad(String bad) {
        this.bad = bad;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getGood() {
        return good;
    }

    public void setGood(String good) {
        this.good = good;
    }

    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }
}
