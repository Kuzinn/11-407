package ru.kpfu.itis.form_validation;

import java.util.List;

/**
 * Created by kuzin on 12.04.2016.
 */
public class PopupFormValidationResponse {

    private String status;
    private List errorMessageList;

    public String getStatus() {
        return status;
    }
    public void setStatus(String status) {
        this.status = status;
    }
    public List getErrorMessageList() {
        return this.errorMessageList;
    }
    public void setErrorMessageList(List errorMessageList) {
        this.errorMessageList = errorMessageList;
    }
}
