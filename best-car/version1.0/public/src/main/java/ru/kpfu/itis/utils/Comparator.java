package ru.kpfu.itis.utils;

import ru.kpfu.itis.api.domain.Compare;
import ru.kpfu.itis.api.domain.Favourites;
import ru.kpfu.itis.api.domain.interfaces.WishList;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by kuzin on 30.03.2016.
 */
public class Comparator {

//    public static List<WishList> splitThemTogether(List<? extends WishList> f1, List<? extends WishList> f2){
//        List<WishList> result= (List<WishList>) f2;
//        if(f2!=null && f1!=null){
//            List<WishList> differ=Collections.synchronizedList(new ArrayList<WishList>(
//                    getDifference(Collections.synchronizedList(f2),Collections.synchronizedList(f1))
//            ));
//            for(WishList favs:differ){
//                result.add(favs);
//            }
//        }else if(f1!=null){
//            result= (List<WishList>) f1;
//        }else if(f2!=null){
//            result= (List<WishList>) f2;
//        }else{
//            return new ArrayList<WishList>();
//        }
//        return result;
//    }
//    public static List<WishList> getDifference(List<? extends WishList> f1,List<? extends WishList> f2){
//        List<WishList> result=new ArrayList<WishList>();
//        if((f2.size()>0)&&(f1.size()>0)){
//            List<WishList> difference= (List<WishList>) f2;
//            for(WishList favo: f1){
//                for(WishList res:f2){
//                    if(res.getCar().getId().equals(favo.getCar().getId())){
//                        difference.remove(res);
//                    }
//                }
//
//            }
//            for(WishList favourites:difference){
//                result.add(favourites);
//            }
//        }else{
//            if((f1==null || f1.size()==0)&&(f2.size()>0)){
//                result= (List<WishList>) f2;
//            }else if(f1!=null && f1.size()>0){
//                result= (List<WishList>) f1;
//            }else{
//                return result;
//            }
//        }
//
//        return result;
//    }
    public static List<Compare> splitThemTogetherCom(List<Compare> f1, List<Compare> f2){
        List<Compare> result=f2;
        if(f2!=null && f1!=null){
            List<Compare> differ=Collections.synchronizedList(new ArrayList<Compare>(
                    getDifferenceCom(Collections.synchronizedList(f2),Collections.synchronizedList(f1))
            ));
            for(Compare favs:differ){
                result.add(favs);
            }
        }else if(f1!=null){
            result=f1;
        }else if(f2!=null){
            result=f2;
        }else{
            return new ArrayList<Compare>();
        }
        return result;
    }
    public static List<Compare> getDifferenceCom(List<Compare> f1, List<Compare> f2){
        List<Compare> result=new ArrayList<Compare>();
        if((f2.size()>0)&&(f1.size()>0)){
            List<Compare> difference=new ArrayList<Compare>(f2);
            for(Compare favo: f1){
                for(WishList res:f2){
                    if(res.getCar().getId().equals(favo.getCar().getId())){
                        difference.remove(res);
                    }
                }

            }
            for(Compare favourites:difference){
                result.add(favourites);
            }
        }else{
            if((f1==null || f1.size()==0)&&(f2.size()>0)){
                result=f2;
            }else if(f1!=null && f1.size()>0){
                result=f1;
            }else{
                return result;
            }
        }

        return result;
    }

    public static List<Favourites> splitThemTogetherFav(List<Favourites> f1, List<Favourites> f2){
        List<Favourites> result=f2;
        if(f2!=null && f1!=null){
            List<Favourites> differ=Collections.synchronizedList(new ArrayList<Favourites>(
                    getDifferenceFav(Collections.synchronizedList(f2),Collections.synchronizedList(f1))
            ));
            for(Favourites favs:differ){
                result.add(favs);
            }
        }else if(f1!=null){
            result=f1;
        }else if(f2!=null){
            result=f2;
        }else{
            return new ArrayList<Favourites>();
        }
        return result;
    }
    public static List<Favourites> getDifferenceFav(List<Favourites> f1, List<Favourites> f2){
        List<Favourites> result=new ArrayList<Favourites>();
        if((f2.size()>0)&&(f1.size()>0)){
            List<Favourites> difference=new ArrayList<Favourites>(f2);
            for(Favourites favo: f1){
                for(WishList res:f2){
                    if(res.getCar().getId().equals(favo.getCar().getId())){
                        difference.remove(res);
                    }
                }

            }
            for(Favourites favourites:difference){
                result.add(favourites);
            }
        }else{
            if((f1==null || f1.size()==0)&&(f2.size()>0)){
                result=f2;
            }else if(f1!=null && f1.size()>0){
                result=f1;
            }else{
                return result;
            }
        }

        return result;
    }
}
