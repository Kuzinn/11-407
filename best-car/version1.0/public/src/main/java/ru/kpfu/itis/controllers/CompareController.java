package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.Compare;
import ru.kpfu.itis.api.domain.Equipment;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;
import ru.kpfu.itis.utils.Comparator;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class CompareController extends BaseController{
    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private CompareService compareService;


    @ResponseBody
    @RequestMapping(value = "/add-to-compare", method = RequestMethod.POST)
    public String addToCompare(Long addingcar,Model model){
        Compare favourites=new Compare();
        Car car=carService.getCarById(addingcar);
        favourites.setCar(car);
        car.increasePopularityByComparison();
        carService.updateCar(car);
        if(request.getRemoteUser()!=null){
            User user=userService.getUserByName(request.getUserPrincipal().getName());
            favourites.setUser(user);

            for(Compare userFavs:compareService.getAllComparesesFor(user)){
                if(userFavs.getCar().getId().equals(addingcar)){
                    model.addAttribute("compare",request.getSession().getAttribute("compare"));
                    return "alreadyexists";
                }
            }

            compareService.addCompare(favourites);

        }
        List<Compare> favouritesList;
        if(request.getSession().getAttribute("compare")!=null) {
            for(Compare sessionFav:(List<Compare>) request.getSession().getAttribute("compare")){
                if(sessionFav.getCar().getId().equals(addingcar)){
                    model.addAttribute("compare",request.getSession().getAttribute("compare"));
                    return "alreadyexists";
                }
            }
            favouritesList=(List<Compare>) request.getSession().getAttribute("compare");
            if(request.getRemoteUser()!=null){
                favouritesList= Comparator.splitThemTogetherCom(favouritesList,compareService.getAllComparesesFor(
                        userService.getUserByName(request.getUserPrincipal().getName())
                ));
            }
        }else{
            favouritesList=new ArrayList<Compare>();
        }

        int count=(Integer)request.getSession().getAttribute("compareCount");
        request.getSession().setAttribute("compareCount",++count);
        model.addAttribute("compareCount",++count);

        favouritesList.add(favourites);
        request.getSession().setAttribute("compare",favouritesList);
        model.addAttribute("compare",favouritesList);


        return "ok";
    }

    @ResponseBody
    @RequestMapping(value = "/compare",method = RequestMethod.POST)
    public String deleteComparing(Long del,Model model){
        List<Compare> favouritesList=(List<Compare>) request.getSession().getAttribute("compare");
        Compare deleted=null;
        for(Compare favourites:favouritesList){
            if(favourites.getCar().getId().equals(del)){
                deleted=favourites;
            }
        }
        if(deleted!=null) {
            favouritesList.remove(favouritesList.indexOf(deleted));
        }
        if(request.getRemoteUser()!=null){
            if(deleted!=null) {
                compareService.deleteCompare(deleted);
            }
        }
        int count=(Integer)request.getSession().getAttribute("compareCount");
        request.getSession().setAttribute("compareCount",++count);
        model.addAttribute("compareCount",--count);

        request.getSession().setAttribute("compare",favouritesList);
        return "ok";
    }

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/compare", method = RequestMethod.GET)
    public String renderComparePage(Model model){
        List<Compare> compareList;
        if(request.getRemoteUser()!=null){
            compareList= (List<Compare>) request.getSession().getAttribute("compare");
            compareList=Comparator.splitThemTogetherCom(compareList,compareService.getAllComparesesFor(
                    userService.getUserByName(request.getUserPrincipal().getName())
            ));
            for(Compare favs:Comparator.getDifferenceCom(
                    compareList,compareService.getAllComparesesFor(userService.getUserByName(request.getUserPrincipal().getName())
                    ))){
                favs.setUser(userService.getUserByName(request.getUserPrincipal().getName()));
                compareService.addCompare(favs);
            }
        }else if(request.getSession().getAttribute("compare")!=null){
            compareList= (List<Compare>) request.getSession().getAttribute("compare");
            if(compareList.size()==0){
                model.addAttribute("emptyCompare",true);
            }
        }else{
            compareList=new ArrayList<Compare>();
            model.addAttribute("emptyCompare",true);
        }
        Map<String,Equipment> equipmentMap=new TreeMap<String, Equipment>();
        for(Compare compare:compareList){
            List<Equipment> equipments=compare.getCar().getEquipments();
            int max=0;//Предполагается что самые навороченные комплектации будут вноситься в бд последними
            Equipment res=null;
            for(Equipment eq:equipments){
                if(eq.getId()>max){
                    res=eq;
                }
            }
            equipmentMap.put(compare.getCar().getId().toString(),res);
        }
        request.getSession().setAttribute("compare",compareList);
        request.getSession().setAttribute("equipmentMap",equipmentMap);
        model.addAttribute("compare",compareList);
        model.addAttribute("equipmentMap",equipmentMap);
        return "compare/compare";
    }

}
