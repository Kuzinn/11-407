package ru.kpfu.itis.aspects;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;


import ru.kpfu.itis.aspects.annotations.PasswordsEqualConstraint;
import ru.kpfu.itis.form_validation.RegistrationFormBean;

public class PasswordsEqualConstraintValidator implements
        ConstraintValidator<PasswordsEqualConstraint, Object> {

//    @Override
    public void initialize(PasswordsEqualConstraint arg0) {
    }

//    @Override
    public boolean isValid(Object candidateForm, ConstraintValidatorContext arg1) {
        RegistrationFormBean form = (RegistrationFormBean) candidateForm;
        return form.getPassword().equals(form.getPasswordtwo());
    }
}
