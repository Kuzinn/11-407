package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.api.domain.*;
import ru.kpfu.itis.api.domain.interfaces.WishList;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;
import ru.kpfu.itis.form_validation.ReviewFormBean;
import ru.kpfu.itis.mail.ssl.Sender;
import ru.kpfu.itis.utils.Converter;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.SimpleDateFormat;
import java.util.*;
import ru.kpfu.itis.utils.Comparator;
import ru.kpfu.itis.utils.Vaildator;

/**
 * Created by kuzin on 11.03.2016.
 */
@Controller
public class PagesController extends BaseController {

    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private FavouritesService favouritesService;

    @Autowired
    private CompareService compareService;

    @Autowired
    private ImageService imageService;

    @Autowired
    private ReviewService reviewService;

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/all-filters", method = RequestMethod.GET)
    public String renderAllFiltersPage(){
        return "all_filters/allfilters";
    }

    @RequestMapping(value = "/checkSomething",method = RequestMethod.GET)
    public String check(){
        List<Car> cars=carService.getAllCarses();
        String result="";
        if(cars.size()>0){
            for(Car car : cars){
                System.out.println(car.getBrand() + ": brand ");
                System.out.println(car.getModel() + ": model ");
                System.out.println(car.getGeneration() + ": generation ");
                System.out.println(car.getStartOfProduction().toString() + ": start of production ");
                System.out.println(car.getImagesURL() + ": image");

            }
            result="main/index";
        }else{
            result="security_pages/signin_failure";
        }
        return result;
    }

    @RequestMapping(value = "/addcars",method = RequestMethod.GET)
    public String addCar(){
        List<String> audiImages=new ArrayList<String>();
        List<String> bmwImages=new ArrayList<String>();
        audiImages.add("\\resources\\img\\cars\\audi\\a4\\2015-\\2015-audi-a4.jpg");
        audiImages.add("\\resources\\img\\cars\\audi\\a4\\2015-\\audi_a4_1.jpg");
        bmwImages.add("\\resources\\img\\cars\\bmw\\m3\\2015-\\bmw-m3-sedan.jpg");
        bmwImages.add("\\resources\\img\\cars\\bmw\\m3\\2015-\\m3_market.jpg");
        carService.createCar(
                //изображение
                audiImages,
                //бренд
                "Audi",
                //модель
                "A4",
                //поколение
                "V(B9)",
                "новая",
                //дата начала производства
                new Date(),
                //дата окончания производства
                null,
                //тип кузова
                "седан",
                //наименование комплектации
                "Design 1.4 TFSI",
                //стоимость в базе
                1870000.0,
                //клиренс
                "160mm",
                //привод
                "Передний",
                //коробка передач
                "механическая, 6-ступенчатая",
                //тип двигателя
                "бензиновый",
                //мощность двигателя
                150,
                //крутящий момент
                250,
                //тормоза
                "Передние: вентилируемые, дисковые, задние: дисковые",
                //разгон до 100 км/ч
                "8,7",
                //расход смешаный
                "5,3",
                //фары
                "ксенон plus (опционально: Audi Matrix)",
                //abs
                true,
                //esp
                true,
                //tractionControl
                false,
                //климатическая установка
                "2 зонный климат-контроль",
                //круиз-контроль
                false,
                //усилитель руля
                "электромеханический",
                //парктроник
                true,
                //датчик дождя
                true,
                //датчик света
                true,
                //обогрев зеркал
                true,
                //омыватель фар
                false,
                //электропривод зеркал
                true,
                //обогрев рулевого колеса
                false,
                //подогрев сидений
                true,
                //люк
                "опционально",
                //число мест
                5,
                //центральный замок
                true,
                //сигнализация
                "опционально",
                //подушки безопасности
                "Для водителя и пассажира",
                //старт-стоп
                true ,
                //обьем топливного бака
                "54 литра",
                //собственная масса
                "1395 кг" ,
                //кожанный салон
                "опционально",
                //проекционный дисплей
                "нет",
                //аудиосистема
                "8 колонок",
                //электронная комбинация приборов
                "опционально" ,
                //система автоматической парковки
                true ,
                //зеркала в цвет кузова
                true,
                //отделка потолка
                "ткань",
                //отделка панели приборов
                "пластик",
                //теплозащитное остекление
                true,
                //адаптивная подвеска
                false,
                //launch control
                false
        );
        carService.createCar(
                //изображение
                bmwImages,
                //бренд
                "BMW",
                //модель
                "M3",
                //поколение
                "F80",
                "новая",
                //дата начала производства
                new Date(),
                //дата окончания производства
                null,
                //тип кузова
                "седан",
                //наименование комплектации
                "3.0 AT",
                //стоимость в базе
                4282000.0,
                //клиренс
                "120mm",
                //привод
                "Задний",
                //коробка передач
                "автомат: робот",
                //тип двигателя
                "бензиновый",
                //мощность двигателя
                431,
                //крутящий момент
                550,
                //тормоза
                "Перфорированные, вентилируемые передние и задние тормозные диски (опционально карбоно-керамические тормозные диски)",
                //разгон до 100 км/ч
                "4,3",
                //расход смешаный
                "8,8",
                //фары
                "ксенон",
                //abs
                true,
                //esp
                true,
                //tractionControl
                true,
                //климатическая установка
                "3 зонный климат-контроль",
                //круиз-контроль
                false,
                //усилитель руля
                "электромеханический",
                //парктроник
                true,
                //датчик дождя
                true,
                //датчик света
                true,
                //обогрев зеркал
                true,
                //омыватель фар
                true,
                //электропривод зеркал
                true,
                //обогрев рулевого колеса
                true,
                //подогрев сидений
                true,
                //люк
                "опционально",
                //число мест
                5,
                //центральный замок
                true,
                //сигнализация
                "опционально",
                //подушки безопасности
                "Для водителя и пассажира",
                //старт-стоп
                true ,
                //обьем топливного бака
                "60 литра",
                //собственная масса
                "1635 кг" ,
                //кожанный салон
                "Nappa",
                //проекционный дисплей
                "нет",
                //аудиосистема
                "8 колонок",
                //электронная комбинация приборов
                "опционально" ,
                //система автоматической парковки
                false ,
                //зеркала в цвет кузова
                true,
                //отделка потолка
                "ткань(опционально: алькантара)",
                //отделка панели приборов
                "пластик",
                //теплозащитное остекление
                true,
                //адаптивная подвеска
                true,
                //launch control
                true
        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\chevrolet\\tahoe\\tahoe_2015.png",
//                //бренд
//                "Chevrolet",
//                //модель
//                "Tahoe",
//                //поколение
//                "JK9",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "Внедорожник"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\citroen\\ds5\\citroen_ds5_31345.jpg",
//                //бренд
//                "Citroen",
//                //модель
//                "DS5",
//                //поколение
//                "JK7",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "хэтчбек"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\fiat\\500\\fiat-500-and-500c-by-1_600x0w.jpg",
//                //бренд
//                "Fiat",
//                //модель
//                "500",
//                //поколение
//                "JK6",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "хэтчбек"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\ford\\focus\\ford_focus.png",
//                //бренд
//                "Ford",
//                //модель
//                "Focus",
//                //поколение
//                "JK5",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "хэтчбек"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\honda\\civic\\10_big.jpg",
//                //бренд
//                "Honda",
//                //модель
//                "Civic",
//                //поколение
//                "JK4",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "хэтчбек"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\hyundai\\i40\\ru_kv_i40_sedan_euro.png",
//                //бренд
//                "Hyundai",
//                //модель
//                "i40",
//                //поколение
//                "JK3",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "седан"
//        );
//        carService.createCarWithoutEquipment(
//                //изображение
//                "\\resources\\img\\cars\\hyundai\\i40\\ru_kv_i40_sedan_euro.png",
//                //бренд
//                "Hyundai",
//                //модель
//                "i40",
//                //поколение
//                "JK3",
//                //дата начала производства
//                new SimpleDateFormat("11.09.2013 09:00"),
//                //дата окончания производства
//                null,
//                //тип кузова
//                "седан"
//        );

        return "main/index";
    }

}
