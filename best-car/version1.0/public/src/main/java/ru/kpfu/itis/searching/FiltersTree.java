package ru.kpfu.itis.searching;


import java.util.List;
import java.util.TreeMap;

/**
 * Created by kuzin on 27.03.2016.
 */
public class FiltersTree {
    Node root=new Node();

    public FiltersTree(){}

    public Node getRoot() {
        return root;
    }

    public void setRoot(Node root) {
        this.root = root;
    }

    public void addRootChild(Node node){
        root.addChild(node);
    }

    public String toSQLQuery(){
        StringBuilder sb=new StringBuilder("select * from car,equipment where car.id=equipment.carId_id ");
        sb.append(makeRecursion(root.getChildren()));
        sb.append(";");
        return sb.toString();
    }

//    public void addTo(String parentName,)

    public String makeRecursion(List<Node> children){
        StringBuilder sb=new StringBuilder();
        for(Node n: children){
            if(n.getChildren().size()>0){
                return sb.append(makeRecursion(n.getChildren())).toString();
            }else{
//                for(Variant variant:n.getVariants()){
//                    if(variant.isNode){
//                        sb.append(makeRecursion(variant.getChildren()));
//                    }else{
//                        sb.append(" and ");
//                        if(variant.getType()==VariantType.CHECKBOX){
//                            for(String value:variant.getValues()) {
//                                sb.append(" ( ");
//                                sb.append(variant.getTable());
//                                sb.append(".");
//                                sb.append(variant.getName());
//                                sb.append(" = ");
//                                sb.append(value);
//                                sb.append(")");
//                                if(variant.getValues().indexOf(value)<variant.getValues().size()-1){
//                                    sb.append(" or ");
//                                }
//                            }
//                        }else if(variant.getType()==VariantType.RADIO){
//                            for(String value:variant.getValues()) {
//                                sb.append(" ( ");
//                                sb.append(variant.getTable());
//                                sb.append(".");
//                                sb.append(variant.getName());
//                                sb.append(" = ");
//                                sb.append(value);
//                                sb.append(")");
//                            }
//                        }else{
//                            sb.append(" ( ");
//                            sb.append(variant.getTable());
//                            sb.append(".");
//                            sb.append(variant.getName());
//                            sb.append(" > ");
//                            sb.append(variant.getValues().get(0));//from
//                            sb.append(" or ");
//                            sb.append(variant.getTable());
//                            sb.append(".");
//                            sb.append(variant.getName());
//                            sb.append(" < ");
//                            sb.append(variant.getValues().get(1));//to
//                            sb.append(")");
//                        }
//                    }
//                }
            }

        }
        return sb.toString();
    }


}
