package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.service.CarService;
import ru.kpfu.itis.api.service.EquipmentService;
import ru.kpfu.itis.api.service.ReviewService;
import ru.kpfu.itis.searching.FiltersList;
import ru.kpfu.itis.searching.Variant;
import ru.kpfu.itis.searching.VariantType;
import ru.kpfu.itis.utils.Converter;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kuzin on 05.04.2016.
 */
@Controller
@RequestMapping(value = "/catalog-ajax")
public class CatalogAjaxController extends BaseController {

    @Autowired
    private CarService carService;

    @Autowired
    private ReviewService reviewService;

    @Autowired
    private EquipmentService equipmentService;

    /**
     *все формы с фильтрами будут слать на урлы типа /имятаблицы-имястолбца
     */

    @RequestMapping(value = "/equipment-price",method = RequestMethod.POST)
    public String money(String money_from,String money_to,Model model){
        FiltersList filters= (FiltersList) request.getSession().getAttribute("filters");
        boolean isVariantFromSession=false;
        Variant variant;
        if(filters==null){
            filters=new FiltersList();
            variant=new Variant("equipment","price", VariantType.TEXT);
        }else if(filters.getVariantByName("price")!=null){
            variant=filters.getVariantByName("price");
            isVariantFromSession=true;
        }else{
            variant=new Variant("equipment","price", VariantType.TEXT);
        }

        if((money_from!=null && money_to!=null) && (money_from.length()>0 && money_to.length()>0)) {
            money_from=money_from.replace(" ","").substring(1).replace(".00","");
            money_to=money_to.replace(" ","").substring(1).replace(".00","");
            variant.updateValue(0,money_from);
            variant.updateValue(1,money_to);
        }else if(money_from!=null && money_from.length()>0){

            money_from=money_from.replace(" ","").substring(1).replace(".00","");
            variant.updateValue(0,money_from);
            try {
                variant.updateValue(1,filters.getVariantByName("price").getValues().get(1));
            }catch (Exception e){
                variant.updateValue(1,null);
            }

        }else if(money_to!=null && money_to.length()>0){
            money_to=money_to.replace(" ","").substring(1).replace(".00","");
            variant.updateValue(1,money_to);
            try {
                variant.updateValue(0,filters.getVariantByName("price").getValues().get(0));
            }catch (Exception e){
                variant.updateValue(0,null);
            }
        }else{
            List<Car> cars=carService.execQuery(filters.toSQLQuery());

            Map<String,Double> ratingMap=new TreeMap<String, Double>();
            for(Car c: cars){
                ratingMap.put(c.getId().toString(),reviewService.getAvgRatingByCarID(c.getId()));
            }
            model.addAttribute("cars",cars);
            model.addAttribute("ratingMap",ratingMap);
            return "catalog/ajaxList";
        }
        if(!isVariantFromSession) {
            filters.add(variant);
        }else{
            filters.update(variant);
        }
        request.getSession().setAttribute("filters",filters);
        List<Car> cars=carService.execQuery(filters.toSQLQuery());

        Map<String,Double> ratingMap=new TreeMap<String, Double>();
        for(Car c: cars){
            ratingMap.put(c.getId().toString(),reviewService.getAvgRatingByCarID(c.getId()));
        }
        model.addAttribute("cars",cars);
        model.addAttribute("ratingMap",ratingMap);
        return "catalog/ajaxList";
    }






    @RequestMapping(value = "/catalog-filter",method = RequestMethod.POST)
    public String catalogFilter(Model model){
        Double priceFrom= (Double) request.getAttribute("money-from");
        Double priceTo= (Double) request.getAttribute("money-to");
        String clearanceFrom= (String) request.getAttribute("clirens-from");
        String clearanceTo= (String) request.getAttribute("clirens-to");
        String driveUnitRWD= (String) request.getAttribute("rwd");
        String driveUnit4WD= (String) request.getAttribute("4wd");
        String driveUnitFWD= (String) request.getAttribute("fwd");
        String transmissionManual= (String) request.getAttribute("manual");
        String engineType= (String) request.getAttribute("");
        int enginePowerFrom=(Integer) request.getAttribute("engine-power-from");
        int enginePowerTo=(Integer)request.getAttribute("engine-power-to");
        int torqueFrom=(Integer)request.getAttribute("torque-from");
        int torqueTo=(Integer)request.getAttribute("torque-to");
        String drumBrakes= (String) request.getAttribute("drum-brakes");
        String diskBrakes= (String) request.getAttribute("disk-brakes");
        String accelerationFrom= (String) request.getAttribute("acceleration-from");
        String accelerationTo= (String) request.getAttribute("acceleration-to");
        String fuelConsumptionFrom= (String) request.getAttribute("fuelcomps-from");
        String fuelConsumptionTo= (String) request.getAttribute("fuelcomps-to");
        String headlightsLaser= (String) request.getAttribute("laser-headlights");
        String headlightsLed= (String) request.getAttribute("led-headlights");
        String headlightsXenon= (String) request.getAttribute("xenon-headlights");
        String headlightsRegular= (String) request.getAttribute("regular-headlights");
        //must return boolean
        String abs= Converter.trueFalseToString(
                (String) request.getAttribute("abs-true"),(String) request.getAttribute("abs-false"));

        //must return boolean
        String esp =Converter.trueFalseToString(
                (String) request.getAttribute("esp-true"),(String) request.getAttribute("esp-false"));
        //must return boolean
        String tractionControl =Converter.trueFalseToString(
                (String) request.getAttribute("tc-true"),(String) request.getAttribute("tc-false"));
        String airConditioningClimat = (String) request.getAttribute("climat-system");
        String airConditioningConditioner = (String) request.getAttribute("conditioner");
        //must return boolean
        String cruiseControl =Converter.trueFalseToString(
                (String) request.getAttribute("cruise-true"),(String) request.getAttribute("cruise-false"));
//        String powerSteering =request.getAttribute("");
//        boolean parktronic =request.getAttribute("");
//        boolean rainSensor =request.getAttribute("");
//        boolean lightSensor =request.getAttribute("");
//        boolean heatedMirrors =request.getAttribute("");
//        boolean headlightWasher =request.getAttribute("");
//        boolean electricMirrors =request.getAttribute("");
//        boolean heatedSteeringWheel =request.getAttribute("");
//        boolean seatHeating =request.getAttribute("");
//        String sunroof =request.getAttribute("");
//        int numberOfSeats =request.getAttribute("");
//        boolean centralLocking =request.getAttribute("");
//        String signaling =request.getAttribute("");

        //часто в бд бывает "опционально"
        String airBag =Converter.trueFalseToString(
                (String) request.getAttribute("airbag-true"),(String) request.getAttribute("airbag-false"));
        //must return boolean
        String startStop =Converter.trueFalseToString(
                (String) request.getAttribute("start-stop-true"),(String) request.getAttribute("start-stop-false"));
//        String fuelTank =request.getAttribute("");
//        String ownWeight =request.getAttribute("");
        String leatherSeats = (String) request.getAttribute("salon-leather");
        //в бд записано по другому
        String projector =Converter.trueFalseToString(
                (String) request.getAttribute("projection-true"),(String) request.getAttribute("projection-false"));
        //в бд записано по другому
        String audioSystem =Converter.trueFalseToString(
                (String) request.getAttribute("audio-true"),(String) request.getAttribute("audio-false"));
        String electricSpeedometer = (String) request.getAttribute("electricdashboard");
//        boolean automaticParkingSystem =request.getAttribute("");
//        boolean bodyColorMirrors =request.getAttribute("");
        String ceilingTextile = (String) request.getAttribute("ceiling-textile");
        String ceilingVel = (String) request.getAttribute("ceiling-vel");
        String ceilingAlc = (String) request.getAttribute("ceiling-alc");
        String torpedoPlastic = (String) request.getAttribute("torpedo-plastic");
        String torpedoLeather = (String) request.getAttribute("torpedo-leather");
//        boolean heatInsulatingGlass =request.getAttribute("");
//        boolean adaptiveSuspension =request.getAttribute("");
//        boolean launchControl =request.getAttribute("");





        return "";
    }



}
