package ru.kpfu.itis.searching;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 27.03.2016.
 */
public class Variant {
    VariantType type;

    /**
     * @param name имя столбца из бд из таблицы Комплектации или Автомобиль
     * @param values значения из бд
     */
    String table;
    String name;
    List<String> values;


    public Variant(){}

    public Variant(String table,String name,VariantType variantType){
        this.table=table;
        this.name=name;
        values=new ArrayList<String>();
        type=variantType;
    }


    public Variant(String name, VariantType type, List<String> values) {
        this.name = name;
        this.type = type;
        this.values = values;

    }

    public void addValue(String value){
        values.add(value);
    }

    public void updateValue(int index,String newValue){
        String[] arr=new String[2];
        if(values!=null && values.size()>0) {
            arr = new String[values.size()];
        }
        values.toArray(arr);
        arr[index]=newValue;
        values=new ArrayList<String>();
        for (int i = 0; i < arr.length; i++) {
            values.add(arr[i]);
        }
    }

    public String getTable() {
        return table;
    }

    public void setTable(String table) {
        this.table = table;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public VariantType getType() {
        return type;
    }

    public void setType(VariantType type) {
        this.type = type;
    }

    public List<String> getValues() {
        return values;
    }

    public void setValues(List<String> values) {
        this.values = values;
    }
}
