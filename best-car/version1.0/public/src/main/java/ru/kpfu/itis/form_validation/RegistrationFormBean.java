package ru.kpfu.itis.form_validation;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;
import ru.kpfu.itis.aspects.annotations.PasswordsEqualConstraint;
import ru.kpfu.itis.aspects.annotations.UniqueNickName;

import javax.validation.constraints.NotNull;

/**
 * Created by kuzin on 12.04.2016.
 */
@PasswordsEqualConstraint(message = "пароли не совпадают")
public class RegistrationFormBean {

    @NotEmpty(message = "Поле обязательно для заполнения")
    @UniqueNickName(message = "Пользователь с таким именем уже существует")
    private String nickname;

    @Email(message = "неправильный email")
    private String email;

    @NotNull
    private String password;

    @NotNull
    private String passwordtwo;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordtwo() {
        return passwordtwo;
    }

    public void setPasswordtwo(String passwordtwo) {
        this.passwordtwo = passwordtwo;
    }
}
