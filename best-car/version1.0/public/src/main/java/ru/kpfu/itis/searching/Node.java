package ru.kpfu.itis.searching;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 27.03.2016.
 */
public class Node {
    List<Node> children;


    /**
     * @param name имя категории в html
     */
    String name;
    List<Variant> variants;

    public Node(){
        variants=new ArrayList<Variant>();
        children=new ArrayList<Node>();
    }

    public Node(String name){
        variants=new ArrayList<Variant>();
        children=new ArrayList<Node>();
    }

    public Node(List<Node> children, String name, List<Variant> variants) {
        this.children = children;
        this.name = name;
        this.variants = variants;
    }

    public void addChild(Node child){
        children.add(child);
    }
    public void addVariant(Variant variant){
        variants.add(variant);
    }
    public List<Node> getChildren() {
        return children;
    }

    public void setChildren(List<Node> children) {
        this.children = children;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }
}
