package ru.kpfu.itis.aspects;

import org.springframework.beans.factory.annotation.Autowired;
import ru.kpfu.itis.api.service.UserService;
import ru.kpfu.itis.aspects.annotations.PasswordsEqualConstraint;
import ru.kpfu.itis.aspects.annotations.UniqueNickName;
import ru.kpfu.itis.form_validation.RegistrationFormBean;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * Created by kuzin on 12.04.2016.
 */
public class UniqueNickNameValidator implements
        ConstraintValidator<UniqueNickName, Object> {

    @Autowired
    private UserService userService;

    public void initialize(UniqueNickName uniqueNickName) {
    }

    public boolean isValid(Object o, ConstraintValidatorContext constraintValidatorContext) {
        String nickname = (String) o;
        return userService.getUserByName(nickname)==null;
    }
}
