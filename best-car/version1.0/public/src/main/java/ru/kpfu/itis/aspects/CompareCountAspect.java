package ru.kpfu.itis.aspects;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.ui.Model;
import ru.kpfu.itis.api.service.CompareService;
import ru.kpfu.itis.api.service.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by kuzin on 02.04.2016.
 */
@Aspect
@Component
public class CompareCountAspect {
//    если в сессии нет количества сравнений
//    положи 0
//    если пользователь авторизован положи количество из бд
    @Autowired
    private HttpServletRequest request;

    @Autowired
    private CompareService compareService;

    @Autowired
    private UserService userService;

    @Pointcut("@annotation(ru.kpfu.itis.aspects.annotations.CompareCountInit)")
    public void compareCountInitMethod() {
    }

    @Before("compareCountInitMethod()")
    public void initCompareCount() {
        if(request.getSession().getAttribute("compareCount")==null){
            request.getSession().setAttribute("compareCount",0);
            request.setAttribute("compareCount",0);
        }
        if(request.getRemoteUser()!=null){
            request.getSession().setAttribute("compareCount",
                    compareService.getAllComparesesFor(
                            userService.getUserByName(request.getUserPrincipal().getName())
                    ).size()
            );
            request.setAttribute("compareCount",
                    compareService.getAllComparesesFor(
                            userService.getUserByName(request.getUserPrincipal().getName())
                    ).size()
            );
        }
    }
}
