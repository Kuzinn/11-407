package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class ItemController extends BaseController {

    @Autowired
    private CarService carService;

    @Autowired
    private ReviewService reviewService;

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/item", method = RequestMethod.GET)
    public String renderItemPage(Long id,Model model){
        Car car=carService.getCarById(id);
        car.increasePopularityByReview();
        carService.updateCar(car);
        List<Review> all=reviewService.getReviewsByCarId(car.getId());
        List<Review> returnableReviewsList=new ArrayList<Review>();
        Integer from=0;
        if(all.size()>0){
            if(all.size()>=2){
                returnableReviewsList.add(all.get(0));
                returnableReviewsList.add(all.get(1));
                if(all.size()>2 && all.subList(2,all.size()).size()>0){
                    from=2;
                }else{
                    from=-1;
                }
            }else{
                returnableReviewsList.add(all.get(0));
                from=1;
            }
        }

        model.addAttribute("revs",returnableReviewsList);
        model.addAttribute("car",car);
        model.addAttribute("from",from);
        model.addAttribute("stars",reviewService.getAvgRatingByCarID(id));
        return "items/item";
    }

    @RequestMapping(value = "/item/loadmore",method = RequestMethod.POST)
    public String loadMoreItems(Long id,Integer from,Model model){
        List<Review> all=reviewService.getReviewsByCarId(carService.getCarById(id).getId());
        List<Review> returnableReviewsList=new ArrayList<Review>();
        if(all.size()>0){
            if(all.subList(from,all.size()).size()>=10){
                for (int i = from; i < from+10; i++) {
                    returnableReviewsList.add(all.get(from+i));
                }
                if(all.subList(from+11,all.size()).size()>0) {
                    model.addAttribute("from", from + 11);
                }else{
                    model.addAttribute("from",-1);
                }
            }else{
                for (int i = 0; i < all.subList(from,all.size()).size(); i++) {
                    returnableReviewsList.add(all.get(from+i));

                }
                model.addAttribute("from",-1);
            }
        }
        model.addAttribute("reviews",returnableReviewsList);
        return "items/ajaxItems";
    }

}
