package ru.kpfu.itis.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by kuzin on 03.04.2016.
 */
public class Vaildator {
    public static boolean registerFormIsNorm(String name,String pass,String passTwo,String email){
        if(name.length()==0) return false;

        if(!pass.equals(passTwo)) return false;

        if(!checkWithRegExp(email)) return false;
        return true;
    }
    public static boolean checkWithRegExp(String str){
        String pattern="^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(str);
        return m.matches();
    }

}
