package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.Favourites;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.service.*;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;
import ru.kpfu.itis.utils.Comparator;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class FavouritesController extends BaseController {
    @Autowired
    private CarService carService;

    @Autowired
    private UserService userService;

    @Autowired
    private FavouritesService favouritesService;


    @ResponseBody
    @RequestMapping(value = "/add-to-favourites", method = RequestMethod.POST)
    public String addToFavourites(Long addingcar,Model model){
        Favourites favourites=new Favourites();
        Car car=carService.getCarById(addingcar);
        favourites.setCar(car);
        car.increasePopularityByFavourite();
        carService.updateCar(car);
        if(request.getRemoteUser()!=null){
            User user=userService.getUserByName(request.getUserPrincipal().getName());
            favourites.setUser(user);

            for(Favourites userFavs:favouritesService.getAllFavouritesFor(user)){
                if(userFavs.getCar().getId().equals(addingcar)){
                    model.addAttribute("favourites",request.getSession().getAttribute("favourites"));
                    return "alreadyexists";
                }
            }

            favouritesService.addFavourites(favourites);

        }
        List<Favourites> favouritesList;
        if(request.getSession().getAttribute("favourites")!=null) {
            for(Favourites sessionFav:(List<Favourites>) request.getSession().getAttribute("favourites")){
                if(sessionFav.getCar().getId().equals(addingcar)){
                    model.addAttribute("favourites",request.getSession().getAttribute("favourites"));
                    return "alreadyexists";
                }
            }
            favouritesList=(List<Favourites>) request.getSession().getAttribute("favourites");
            if(request.getRemoteUser()!=null){
                favouritesList= Comparator.splitThemTogetherFav(favouritesList,favouritesService.getAllFavouritesFor(
                        userService.getUserByName(request.getUserPrincipal().getName())
                ));
            }
        }else{
            favouritesList=new ArrayList<Favourites>();
        }
        favouritesList.add(favourites);
        request.getSession().setAttribute("favourites",favouritesList);
        model.addAttribute("favourites",favouritesList);

        return "ok";
    }

    @ResponseBody
    @RequestMapping(value = "/favourites",method = RequestMethod.POST)
    public String deleteFromFavourites(Long del,Model model){
        List<Favourites> favouritesList=(List<Favourites>) request.getSession().getAttribute("favourites");
        Favourites deleted=null;
        for(Favourites favourites:favouritesList){
            if(favourites.getCar().getId().equals(del)){
                deleted=favourites;
            }
        }
        if(deleted!=null) {
            favouritesList.remove(favouritesList.indexOf(deleted));
        }
        if(request.getRemoteUser()!=null){
            if(deleted!=null) {
                favouritesService.deleteFavourites(deleted);
            }
        }


        request.getSession().setAttribute("favourites",favouritesList);
        return "ok";
    }

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/favourites", method = RequestMethod.GET)
    public String renderFavouritesPage(Model model){
        List<Favourites> favouritesList;
        if(request.getRemoteUser()!=null){
            favouritesList= (List<Favourites>) request.getSession().getAttribute("favourites");
            favouritesList=Comparator.splitThemTogetherFav(favouritesList,favouritesService.getAllFavouritesFor(
                    userService.getUserByName(request.getUserPrincipal().getName())
            ));
            for(Favourites favs:Comparator.getDifferenceFav(
                    favouritesList,favouritesService.getAllFavouritesFor(userService.getUserByName(request.getUserPrincipal().getName())
                    ))){
                favs.setUser(userService.getUserByName(request.getUserPrincipal().getName()));
                favouritesService.addFavourites(favs);
            }
        }else if(request.getSession().getAttribute("favourites")!=null){
            favouritesList= (List<Favourites>) request.getSession().getAttribute("favourites");
            if(favouritesList.size()==0){
                model.addAttribute("emptyFav",true);
            }
        }else{
            favouritesList=new ArrayList<Favourites>();
            model.addAttribute("emptyFav",true);
        }
        request.getSession().setAttribute("favourites",favouritesList);
        model.addAttribute("favourites",favouritesList);
        return "favourite/favourite";
    }


}
