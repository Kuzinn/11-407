package ru.kpfu.itis.aspects.annotations;

import ru.kpfu.itis.aspects.UniqueNickNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by kuzin on 12.04.2016.
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Constraint(validatedBy = UniqueNickNameValidator.class)
public @interface UniqueNickName {

    String message();

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
