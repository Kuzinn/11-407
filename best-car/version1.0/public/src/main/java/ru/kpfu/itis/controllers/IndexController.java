package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.Variables.Rating;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.api.service.CarService;
import ru.kpfu.itis.api.service.ReviewService;
import ru.kpfu.itis.aspects.annotations.CompareCountInit;
import ru.kpfu.itis.aspects.annotations.RegistrationFormBeanInit;
import ru.kpfu.itis.form_validation.RegistrationFormBean;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 * Created by kuzin on 08.03.2016.
 */
@Controller
public class IndexController extends BaseController {

    @Autowired
    private CarService carService;

    @Autowired
    private ReviewService reviewService;

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String renderIndexPage(Model model){

        List<String> allBrands=carService.getAllBrands();
        List<Car> popCars;
        popCars=carService.getPopularCars(2);
        List<Car> newCars;
        newCars=carService.getNewCars(2);

        Map<String,Double> ratingMap=new TreeMap<String, Double>();
        for(Car c:newCars){
            ratingMap.put(c.getId().toString(),reviewService.getAvgRatingByCarID(c.getId()));
        }

        model.addAttribute("compareCount", request.getSession().getAttribute("compareCount"));
        model.addAttribute("newCars",newCars);
        model.addAttribute("popCars",popCars);
        model.addAttribute("ratingMap",ratingMap);

        return "main/index";
    }

    @CompareCountInit
    @RegistrationFormBeanInit
    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String renderIndexPageWithOpenedLoginForm(Model model,boolean errors){
        if(errors) model.addAttribute("errors",errors);
        model.addAttribute("loginMustOpen","open");
        return renderIndexPage(model);
    }


}
