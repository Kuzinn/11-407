package ru.kpfu.itis.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.kpfu.itis.api.service.*;

/**
 * Created by kuzin on 11.04.2016.
 */
@Controller
public class SubsidiarySecurityController extends BaseController {

    @RequestMapping(value = "/signin-failure",method = RequestMethod.GET)
    public String signInFailure(){return "security_pages/signin_failure";}

    @RequestMapping(value = "/signin", method = RequestMethod.GET)
    public String signin() {
        return "redirect:/login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout() {
        return "redirect:/";
    }


}
