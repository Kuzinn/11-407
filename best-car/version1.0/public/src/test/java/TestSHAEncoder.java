import org.junit.Test;
import ru.kpfu.itis.utils.SHAEncoder;

import static org.junit.Assert.fail;

/**
 * Created by kuzin on 13.04.2016.
 */
public class TestSHAEncoder {

    @Test
    public void testEncode(){

        String expected="f9834e8d93559a3c38b4fbad2020626a00659c8f84023d5758a9838fec7f07de";
        if(!SHAEncoder.encode("этот тест никогда не завалится").equals(expected))
            fail("Not equals");
    }
}
