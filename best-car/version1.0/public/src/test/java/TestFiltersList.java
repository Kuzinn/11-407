import org.junit.Test;
import static org.junit.Assert.*;
import ru.kpfu.itis.searching.FiltersList;
import ru.kpfu.itis.searching.Variant;
import ru.kpfu.itis.searching.VariantType;


public class TestFiltersList {

    @Test
    public void testSQLQuery(){
        FiltersList fl=new FiltersList();
        Variant variant=new Variant("equipment","transmission", VariantType.CHECKBOX);
        variant.addValue("механическая");
        fl.add(variant);
        Variant variant1=new Variant("equipment","price",VariantType.TEXT);
        variant1.addValue("150000");
        variant1.addValue("259995");
        fl.add(variant1);
        Variant variant2=new Variant("equipment","abs",VariantType.RADIO);
        variant2.addValue("true");
        fl.add(variant2);
        String expected="select * from car,equipment where car.id=equipment.carId_id  and  ( equipment.transmission = механическая) and  ( equipment.price > 150000 and equipment.price < 259995) and  ( equipment.abs = true)";
        if(!fl.toSQLQuery().equals(expected))
            fail("Not equals");
    }

}
