$(document).ready(function(){
  $(".owl-carousel").owlCarousel({
  	items : 4,
  	margin : 30,
  	nav : true,
  	navText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
  });
$("#gallery li img").hover(function(){
		$('#main-img').attr('src',$(this).attr('src').replace('thumb/', ''));
	});
	var imgSwap = [];
	 $("#gallery li img").each(function(){
		imgUrl = this.src.replace('thumb/', '');
		imgSwap.push(imgUrl);
	});
	  $(".infotoggle").click(function () {
   $("div.infotoggle-text").toggle();
});
	$('.favourite').click(function(){
			var _this=$('.favourite');
			if(!_this.hasClass("favactive")){

				$.ajax({
					type: "POST",
					url: "/add-to-favourites",
					data: {
						addingcar: _this.children("a").attr('id')
					}
				}).done(function (data) {  // сюда приходит ответ при успехе
					if (data == 'ok') {
						_this.addClass("favactive");
					}else if(data=="alreadyexists"){
						alert("You already has it");
					}
				}).fail(function () {      // сюда приходит ответ если на сервере прооизошла ошибка
					alert("Ooops. Something went wrong.");
				});


			}


	});
	if($(".from-review").text()==-1){
		$(".reviews-showmore").hide();
	}

	$(".reviews-showmore").click(function(){
		var $this = $(this);
		$.ajax({
			type: "POST",
			url: "/item/loadmore",
			data: {
				id: $('.favourite').children("a").attr('id'),
				from: $(".from-review").text()
			}
		}).done(function (data) {  // сюда приходит ответ при успехе
			//console.log(data);
			if (data != '') {
				$(".reviews-usrrev").last().after(data);
				if($(".from-review").first().text()==-1){
					$(".reviews-showmore").hide();
				}
			} else {
				$this.hide();
			}
		}).fail(function () {
			$this.hide();
		});
	});


	$('.compare').click(function(){
		var _this=$('.compare');
		if(!_this.hasClass("favactive")){

			$.ajax({
				type: "POST",
				url: "/add-to-compare",
				data: {
					addingcar: _this.children("a").attr('id')
				}
			}).done(function (data) {  // сюда приходит ответ при успехе
				if (data == 'ok') {
					_this.addClass("favactive");
				}else if(data=="alreadyexists"){
					alert("You already has it");
				}
			}).fail(function () {      // сюда приходит ответ если на сервере прооизошла ошибка
				alert("Ooops. Something went wrong.");
			});


		}


	});

});
