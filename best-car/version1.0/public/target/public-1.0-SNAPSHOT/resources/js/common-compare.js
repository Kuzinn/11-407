$(document).ready(function(){
  $(".item").on("click", function(){
    var that=$(this);
    var myIndex =$(this).parent().index();
    $.ajax({
      type: "POST",
      url: "/compare",
      data: {
        del: myIndex
      }
    }).done(function (data) {  // сюда приходит ответ при успехе
      if (data == 'ok') {
        $(that).parents("table").find("tr").each(function () {
          $(this).find("th:eq(" + myIndex + ")").remove();
          $(this).find("td:eq(" + myIndex + ")").remove();
        });
      }
    }).fail(function () {      // сюда приходит ответ если на сервере прооизошла ошибка
      alert("Ooops. Something went wrong.")
    });
  });
  $('table').stickyTableHeaders();
  $("#all-param").click(function(){
    $(this).addClass("back-active");
    $("#diff-param").removeClass("back-active");
    var mainArray=$('#table-body').children().toArray();
    for(var i=0;i<mainArray.length;i++){
      $(mainArray[i]).css("display","table-row");
    }
  });

  $("#diff-param").click(function(){
    $(this).addClass("back-active");
    $("#all-param").removeClass("back-active");
    var mainArray=$('#table-body').children().toArray();
    for(var i=0;i<mainArray.length;i++){
      var param=$(mainArray[i]).children().toArray()[1];

      var arr=$(mainArray[i]).children().toArray();
      var equalsElemCount=1;

      for(var j=2;j<arr.length;j++){
        if($(arr[j]).text()==$(param).text()){
          equalsElemCount+=1;
        }
      }
      if(equalsElemCount==arr.length-1){
        $(mainArray[i]).css("display","none");
      }
    }

  });
});

