$(function() {
    $( "#slider-range-one" ).slider({
        range: true,
        min: 1960,
        step: 1,
        max: 2016,
        values: [ 2010, 2016 ],
        slide: function( event, ui ) {
            $( "#amount-one" ).html( ui.values[ 0 ]  + " - " + ui.values[ 1 ]  );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-one" ).html( $( "#slider-range-one").slider( "values", 0 ) +" км."+
        " - " + $( "#slider-range-one" ).slider( "values", 1 ) + " км.");
});
$(function() {
    $( "#slider-range-two" ).slider({
        range: true,
        min: 0,
        step: 5000,
        max: 500000,
        values: [ 0, 5000 ],
        slide: function( event, ui ) {
            $( "#amount-two" ).html( ui.values[ 0 ] + " км." + " - " + ui.values[ 1 ] + " км." );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-two" ).html( $( "#slider-range-two").slider( "values", 0 ) +" км."+
        " - " + $( "#slider-range-two" ).slider( "values", 1 ) + " км.");
});
$(function() {
    $( "#slider-range-three" ).slider({
        range: true,
        min: 1,
        step: 5,
        max: 1000,
        values: [ 70, 110 ],
        slide: function( event, ui ) {
            $( "#amount-three" ).html( ui.values[ 0 ] + " л.с" + " - " + ui.values[ 1 ] + " л.с" );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-three" ).html( $( "#slider-range-three").slider( "values", 0 ) +" л.с"+
        " - " + $( "#slider-range-three" ).slider( "values", 1 ) + " л.с");
});
$(function() {
    $( "#slider-range-four" ).slider({
        range: true,
        min: 0,
        step: 1,
        max: 5,
        values: [ 0, 5 ],
        slide: function( event, ui ) {
            $( "#amount-four" ).html( " от " + ui.values[ 0 ] + " - до " + ui.values[ 1 ] );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-four" ).html( " от " + $( "#slider-range-four" ).slider( "values", 0 ) +
        " - до " + $( "#slider-range-four" ).slider( "values", 1 ) );
});
$(function() {
    $( "#slider-range-five" ).slider({
        range: true,
        min: 10,
        step: 10,
        max: 250,
        values: [ 100, 170 ],
        slide: function( event, ui ) {
            $( "#amount-five" ).html( " от " + ui.values[ 0 ] + " мм. - до " + ui.values[ 1 ] + " мм.");
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-five" ).html( " от " + $( "#slider-range-five" ).slider( "values", 0 ) +" мм."+
        " - до " + $( "#slider-range-five" ).slider( "values", 1 ) +" мм.");
});
$(function() {
    $( "#slider-range-six" ).slider({
        range: true,
        min: 1,
        step: 5,
        max: 1000,
        values: [ 70, 110 ],
        slide: function( event, ui ) {
            $( "#amount-six" ).html( ui.values[ 0 ] + "Н.м" + " - " + ui.values[ 1 ] + " Н.м" );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-six" ).html( $( "#slider-range-six").slider( "values", 0 ) +" Н.м"+
        " - " + $( "#slider-range-six" ).slider( "values", 1 ) + " Н.м");
});
$(function() {
    $( "#slider-range-seven" ).slider({
        range: true,
        min: 2.0,
        step: 0.1,
        max: 17.0,
        values: [ 8.0, 13.0 ],
        slide: function( event, ui ) {
            $( "#amount-seven" ).html( ui.values[ 0 ] + "c." + " - " + ui.values[ 1 ] + "c." );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-seven" ).html( $( "#slider-range-seven").slider( "values", 0 ) +" c."+
        " - " + $( "#slider-range-seven" ).slider( "values", 1 ) + " c.");
});
$(function() {
    $( "#slider-range-eight" ).slider({
        range: true,
        min: 2.0,
        step: 0.1,
        max: 50.0,
        values: [ 6.0, 11.0 ],
        slide: function( event, ui ) {
            $( "#amount-eight" ).html( ui.values[ 0 ] + "л./100км." + " - " + ui.values[ 1 ] + "л./100км." );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-eight" ).html( $( "#slider-range-eight").slider( "values", 0 ) +" л./100км."+
        " - " + $( "#slider-range-eight" ).slider( "values", 1 ) + " л./100км.");
});
$(function() {
    $( "#slider-range-nine" ).slider({
        range: true,
        min: 700,
        step: 10,
        max: 3500,
        values: [ 1000, 1500 ],
        slide: function( event, ui ) {
            $( "#amount-nine" ).html( ui.values[ 0 ] + "кг." + " - " + ui.values[ 1 ] + "кг." );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-nine" ).html( $( "#slider-range-nine").slider( "values", 0 ) +" кг."+
        " - " + $( "#slider-range-nine" ).slider( "values", 1 ) + " кг.");
});
$(function() {
    $( "#slider-range-ten" ).slider({
        range: true,
        min: 12.0,
        step: 0.5,
        max: 150.0,
        values: [ 35.0, 55.0 ],
        slide: function( event, ui ) {
            $( "#amount-ten" ).html( ui.values[ 0 ] + "л." + " - " + ui.values[ 1 ] + "л." );
            $( "#amount1" ).val(ui.values[ 0 ]);
            $( "#amount2" ).val(ui.values[ 1 ]);
        }
    });
    $( "#amount-ten" ).html( $( "#slider-range-ten").slider( "values", 0 ) +" л."+
        " - " + $( "#slider-range-ten" ).slider( "values", 1 ) + " л.");
});