$(function() {

	//SVG Fallback
	if(!Modernizr.svg) {
		$("img[src*='svg']").attr("src", function() {
			return $(this).attr("src").replace(".svg", ".png");
		});
	};

	//
	//$(".srch-box ~ h2,h3").onfocus(function() { //Change
	//	var th = $(this);
	//	$.ajax({
	//		type: "POST",
	//		url: "/register", //Change
	//		data: th.serialize()
	//	}).done(function() {
	//		$.magnificPopup.close();
	//		setTimeout(function() {
	//			// Done Functions
	//			th.trigger("reset");
	//		}, 1000);
	//	});
	//	return false;
	//});

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$("img, a").on("dragstart", function(event) { event.preventDefault(); });

});


$(document).ready(function(){
	 $(".toggle-mnu").mouseover(function(){
  	$(".top-mnu-myroom").toggle();
  });
	 $(".top-mnu-myroom").mouseleave(function(){
  	$(".top-mnu-myroom").toggle();
  });
    $(".login-popup").magnificPopup();
  $(".registration-popup").magnificPopup();
  $(".js_in").focusin(function(){
  	$(this).next().css( "width", "80%" );
  });
  $(".js_in").focusout(function(){
  	$(this).next().css( "width", "0%" );
  });
  $(".js_in2").focusin(function(){
  	$(this).next().css( "width", "80%" );
  });
  $(".js_in2").focusout(function(){
  	$(this).next().css( "width", "0%" );
  });

    if($("#login").hasClass("open")){
        $(".login-popup").magnificPopup('open');
    }


	$("#reg-email").blur(function(){
		var s=$(this).val();
		if( (s.length>0) && (!validateEmail(s)) ){
			var attr = $("#reg-butn").attr('disabled');
			if (typeof attr == typeof undefined || attr == false) {
				$("#reg-butn").attr('disabled','disabled');
				$("#reg-email").parent().after("<div id='wrong-email' class='wrong'>Неверный формат почты!</div>");
			}
		}else{
			if($("#reg-butn").filter("[disabled='disabled']")){
				$("#reg-butn").removeAttr('disabled');
				$("#wrong-email").remove();
			}
		}
	});
	$("#reg-2pass").blur(function(){
		if($("#reg-2pass").val()!=$("#reg-pass").val() && !$("#wrong-2pass").length>0){
			$("#reg-butn").attr('disabled','disabled');
			$("#reg-2pass").parent().after("<div id='wrong-2pass' class='wrong'>Пароли не совпадают!</div>");
		}else{
			if($("#reg-2pass").val().length>0){
				if($("#reg-2pass").val()==$("#reg-pass").val()){
					$("#wrong-2pass").remove();
					$("#reg-butn").removeAttr('disabled');
				}
			}
		}
	});

	function validateEmail(email) {
		var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
		return re.test(email);
	}
    $("._price").replaceWith(function(){
        var that=$(this);
        return ' <div><span>&#8381;</span> '+leftpad($(that).text().match(/\d+/))+'</div>';
    });
    $("._price-from").replaceWith(function(){
        var that=$(this);
        return ' <div>от <span>&#8381;</span> '+leftpad($(that).text().match(/\d+/))+'</div>';
    });

    function leftpad(s){
        var arr=s.toString().split('');
        var result="";
        var count=0;
        for(var i=arr.length-1;i>=0;i--){

            result=arr[i]+result;
            count++;
            if(count==3){
                result=" "+result;
                count=0;
            }
        }


        return result;
    }
});



