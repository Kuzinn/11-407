<#include "../templates/mainTemplate.ftl">
<@mainTemplate title="Поиск" styles=["css/fonts.min.css","css/header.min.css", "css/catalog.min.css","libs/magnific-popup/magnific-popup.css","css/jquery-ui.min.css","css/secondaryfooter.min.css"]
scripts=["libs/checkbox/checkbox.js","libs/autoNumeric/autoNumeric-min.js","libs/magnific-popup/jquery.magnific-popup.min.js","js/jquery-ui.min.js" ,"js/common-catalog.js","js/some.js","js/ajax-catalog.js"  ] />
<#-- @ftlvariable name="car" type="ru.kpfu.itis.api.domain.Car" -->


<#macro m_body>
<section>
    <div class="container">
        <div class="row">
            <h5>
                Сортировать по : <a href="#">цене</a> <a href="#">популярности</a> <a href="#">рейтингу</a>
            </h5>
        </div>
        <div class="row">
            <div class="col-lg-9">
                <#if cars??>
                    <div id="extradition">
                        <#list cars as car>
                            <#include "catalogCarItem.ftl">
                            <@catalogCar car=car ratingMap=ratingMap/>
                        </#list>
                    </div>
                    <#else >
                        <h2>Мы не знаем таких машин!</h2>
                </#if>

            </div>
            <div class="col-lg-3">
                <form class="srch-box" >
                    <aside class="accordion">
                        <h1>Цена</h1>
                        <div class="srch-box-price first opened">
                            <form id="money">
                            <span>от</span>
                            <input class="_money moneyfrom" id="money-from" name="money-from" type="text" placeholder="${moneyFrom}">
                            <span>до</span>
                            <input class="_money moneyto" id="money-to" name="money-to" type="text" placeholder="${moneyTo}">
                            </form>
                        </div>

                        <h1>Общие характеристики</h1>
                        <div>
                            <h2 class="first">Производитель</h2>
                            <div class="srch-box-vendr expanded">
                                <table>
                                    <tr>
                                        <td>
                                            <label for="checkbox2" class="srch-box-chboxlabel">
                                                <input name="_Audi" id="checkbox2" type="checkbox" value="Audi" ${_Audi!''}>
                                                <#--<input name="_Audi" id="checkbox2" type="checkbox" value="Audi" ${_Audi!''} <#if param?contains(item)>checked</#if> >-->
                                            </label>
                                            <span>Audi</span>
                                        </td>
                                        <td>
                                            <label for="checkbox3" class="srch-box-chboxlabel">
                                                <input name="_Honda" id="checkbox3" type="checkbox" value="Honda" ${_Honda!''}>
                                            </label>
                                            <span>Honda</span>
                                        </td>
                                        <td>
                                            <label for="checkbox5" class="srch-box-chboxlabel">
                                                <input name="_Peugeot" id="checkbox5" type="checkbox" value="Peugeot" ${_Peugeot!''}>
                                            </label>
                                            <span>Peugeot</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="checkbox2" class="srch-box-chboxlabel">
                                                <input name="_BMW" id="checkbox2" type="checkbox" value="BMW" ${_BMW!''}>
                                            </label>
                                            <span>BMW</span>
                                        </td>
                                        <td>
                                            <label for="checkbox3" class="srch-box-chboxlabel">
                                                <input name="_Hyundai" id="checkbox3" type="checkbox" value="Hyundai" ${_Hyundai!''}>
                                            </label>
                                            <span>Hyundai</span>
                                        </td>
                                        <td>
                                            <label for="checkbox5" class="srch-box-chboxlabel">
                                                <input name="_Skoda" id="checkbox5" type="checkbox" value="Skoda" ${_Skoda!''}>
                                            </label>
                                            <span>Skoda</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="checkbox2" class="srch-box-chboxlabel">
                                                <input name="_Chevrolet" id="checkbox2" type="checkbox" value="Chevrolet" ${_Chevrolet!''}>
                                            </label>
                                            <span>Chevrolet</span>
                                        </td>
                                        <td>
                                            <label for="checkbox4" class="srch-box-chboxlabel">
                                                <input name="_Kia" id="checkbox4" type="checkbox" value="Kia" ${_Kia!''}>
                                            </label>
                                            <span>Kia</span>
                                        </td>
                                        <td>
                                            <label for="checkbox5" class="srch-box-chboxlabel">
                                                <input name="_Toyota" id="checkbox5" type="checkbox" value="Toyota" ${_Toyota!''}>
                                            </label>
                                            <span>Toyota</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="checkbox2" class="srch-box-chboxlabel">
                                                <input name="_Citroen" id="checkbox2" type="checkbox" value="Citroen" ${_Citroen!''}>
                                            </label>
                                            <span>Citroen</span>
                                        </td>
                                        <td>
                                            <label for="checkbox4" class="srch-box-chboxlabel">
                                                <input name="_Lada" id="checkbox4" type="checkbox" value="Lada" ${_Lada!''}>
                                            </label>
                                            <span>Lada</span>
                                        </td>
                                        <td>
                                            <label for="checkbox5" class="srch-box-chboxlabel">
                                                <input name="_Volkswagen" id="checkbox5" type="checkbox" value="Volkswagen" ${_Volkswagen!''}>
                                            </label>
                                            <span>Volkswagen</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="checkbox3" class="srch-box-chboxlabel">
                                                <input name="_Fiat" id="checkbox3" type="checkbox" value="Fiat" ${_Fiat!''}>
                                            </label>
                                            <span>Fiat</span>
                                        </td>
                                        <td>
                                            <label for="checkbox4" class="srch-box-chboxlabel">
                                                <input name="_Mercedes" id="checkbox4" type="checkbox" value="Mercedes" ${_Mercedes!''}>
                                            </label>
                                            <span>Mercedes</span>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="checkbox3" class="srch-box-chboxlabel">
                                                <input name="_Ford" id="checkbox3" type="checkbox" value="Ford" ${_Ford!''}>
                                            </label>
                                            <span>Ford</span>
                                        </td>
                                        <td>
                                            <label for="checkbox4" class="srch-box-chboxlabel">
                                                <input name="_Nissan" id="checkbox4" type="checkbox" value="Nissan" ${_Nissan!''}>
                                            </label>
                                            <span>Nissan</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><br></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="checkbox2" class="srch-box-chboxlabel">
                                                <input name="_Porsche" id="checkbox2" type="checkbox" value="Porsche" ${_Porsche!''}>
                                            </label>
                                            <span>Porsche</span>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <h2>Состояние</h2>
                            <div class="srch-box-condi">

                                <label for="checkbox0" class="srch-box-chboxlabel">
                                    <input id="checkbox0" name="from-store" type="checkbox" value="новая" ${fromStore}>
                                </label>
                                <span>новая</span>
                                <label for="checkbox1" class="srch-box-chboxlabel js_customHeader">
                                    <input id="checkbox1" name="used" type="checkbox" value="б.у" ${used}>
                                </label>
                                <span>б.у</span>
                                <div class="js_customHeaderDiv">

                                    <h3>Год выпуска <span id="amount-one"></span></h3>
                                    <div id="slider-range-one" class="someslider">



                                        <form method="post" >
                                            <input name="year-from" type="hidden" id="amount1">
                                            <input name="year-to" type="hidden" id="amount2">
                                        </form>

                                    </div>
                                    <h3>Пробег <span id="amount-two"></span></h3>
                                    <div id="slider-range-two" class="someslider">
                                        <form method="post">
                                            <input name="mileage-from" type="hidden" id="amount1">
                                            <input name="mileage-to" type="hidden" id="amount2">
                                        </form>
                                    </div>

                                </div>
                            </div>

                            <h2>Тип двигателя</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="gasoline-type" id="checkbox3" type="checkbox" value="Бензиновый">
                                </label>
                                <span>Бензиновый</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="diesel-type" id="checkbox3" type="checkbox" value="Дизельный">
                                </label>
                                <span>Дизельный</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="hybrid-type" id="checkbox3" type="checkbox" value="Гибридный">
                                </label>
                                <span>Гибридный</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="electric-type" id="checkbox3" type="checkbox" value="Электрический">
                                </label>
                                <span>Электрический</span>
                            </div>
                            <h2>Мощность двигателя <span id="amount-three"></span></h2>
                            <div id="slider-range-three" class="someslider">

                                <form method="post">
                                    <input name="engine-power-from" type="hidden" id="amount1">
                                    <input name="engine-power-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Крутящий момент <span id="amount-six"></span></h2>
                            <div id="slider-range-six" class="someslider">

                                <form method="post">
                                    <input name="torque-from" type="hidden" id="amount1">
                                    <input name="torque-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Разгон 0-100 км./ч. <span id="amount-seven"></span></h2>
                            <div id="slider-range-seven" class="someslider">

                                <form method="post">
                                    <input name="acceleration-from" type="hidden" id="amount1">
                                    <input name="acceleration-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Коробка передач</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="manual" type="checkbox" value="Ручная" ${manual!''}>
                                </label>
                                <span>Ручная</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="automatic" type="checkbox" value="Автомат" ${automatic!''}>
                                </label>
                                <span>Автомат</span>
                            </div>
                            <h2>Привод</h2>
                            <div class="srch-box-drve">
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="fwd" type="checkbox" value="Передний">
                                </label>
                                <span>Передний</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="4wd" type="checkbox" value="Полный">
                                </label>
                                <span>Полный</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="rwd" type="checkbox" value="Задний">
                                </label>
                                <span>Задний</span>
                            </div>
                            <h2>Расход <span id="amount-eight"></span></h2>
                            <div id="slider-range-eight" class="someslider">

                                <form method="post">
                                    <input name="fuelcomps-from" type="hidden" id="amount1">
                                    <input name="fuelcomps-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Собственный вес <span id="amount-nine"></h2>
                            <div id="slider-range-nine" class="someslider">

                                <form method="post">
                                    <input name="own-weight-from" type="hidden" id="amount1">
                                    <input name="own-weight-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Обьем топливного бака<span id="amount-ten"></h2>
                            <div id="slider-range-ten" class="someslider">

                                <form method="post">
                                    <input name="fuel-tank-value-from" type="hidden" id="amount1">
                                    <input name="fuel-tank-value-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Люк на крыше</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="luke-roof" type="checkbox" value="Передний">
                                </label>
                                <span>Люк</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="panorama-roof" type="checkbox" value="Передний">
                                </label>
                                <span>Панорама</span>
                            </div>
                        </div>
                        <h1>Безопасность</h1>
                        <div>
                            <h2>Подушки безопасности</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="airbag-true" id="checkbox5" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="airbag-false" id="checkbox5" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Баллы по euroncap  <span id="amount-four"></span></h2>
                            <div id="slider-range-four" class="someslider">

                                <form method="post">
                                    <input name="euroncap-from" type="hidden" id="amount1">
                                    <input name="euroncap-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>ABS</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="abs-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="abs-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>ESP</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="esp-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="esp-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Traction Control</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="tc-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="tc-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Центральный замок</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="central-locking-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="central-locking-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Сигнализация</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="built-in-signaling" id="checkbox3" type="checkbox" value="Есть">
                                </label>
                                <span>Встроенная</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="Centramax-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Centramax</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="Pandora-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Pandora</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="Scher-Khan-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Scher-Khan</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="Sheriff-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Sheriff</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="StarLine-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>StarLine</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="Tomahawk-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Tomahawk</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="other-signaling" id="checkbox3" type="checkbox" value="Нет">
                                </label>
                                <span>Другая</span>
                            </div>
                        </div>
                        <h1>Интерьер</h1>
                        <div>
                            <h2>Число мест</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="number-of-seats-2" type="checkbox" value="2">
                                </label>
                                <span>2</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="number-of-seats-4" type="checkbox" value="4">
                                </label>
                                <span>4</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="number-of-seats-5" type="checkbox" value="5">
                                </label>
                                <span>5</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="checkbox5" name="number-of-seats-8" type="checkbox" value="8">
                                </label>
                                <span>8</span>
                            </div>
                            <h2>Подлокотник</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="armchair-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="armchair-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Обшивка салона</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="salon-textile" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Ткань</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="salon-leather" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Кожа</span>
                            </div>
                            <h2>Обшивка потолка</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="ceiling-textile" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Ткань</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="ceiling-vel" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Велюр</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="ceiling-alc" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Алькантара</span>
                            </div>
                            <h2>Отделка панелей</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="pannels-plastic" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Пластик</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="pannels-carbon" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Карбон</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="pannels-wood" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Дерево</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="pannels-metal" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Метал</span>
                            </div>
                            <h2>Отделка торпедо</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="torpedo-plastic" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Пластик</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="torpedo-leather" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Кожа</span>
                            </div>
                            <h2>Массаж</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="massage-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="massage-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                        </div>
                        <h1>Экстерьер</h1>
                        <div>
                            <h2>Зеркала в цвет кузова</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="body-color-mirrors-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="body-color-mirrors-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Фары</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="regular-headlights" type="radio" value="Есть">
                                </label>
                                <span>Обычные</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" nmae="xenon-headlights" type="radio" value="Есть">
                                </label>
                                <span>Ксеноновые</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="led-headlights" type="radio" value="Есть">
                                </label>
                                <span>LED</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="laser-headlights" type="radio" value="Есть">
                                </label>
                                <span>Лазерные</span>
                            </div>
                        </div>
                        <h1>Мультимедиа</h1>
                        <div>
                            <h2>Аудио-подкотовка</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="audio-true" id="chtruex5" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="audio-false" id="chfalsex5" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Навигация</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="navi-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="navi-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Широкоформатный дисплей мультимедиа</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="modrnscreen-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="modrnscreen-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Дисплеи в подголовниках</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="seat-display-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="seat-display-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Электронный щиток приборов</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="electricdashboard-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="electricdashboard-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Проекция на лобовое стекло</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="projection-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="projection-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Apple CarPlay/Android Auto</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="carplay-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="carplay-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                        </div>
                        <h1>Платформа</h1>
                        <div>
                            <h2>Адаптивная подвеска</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="adaprive-suspesion-true" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chfalsex5" name="adaprive-suspesion-false" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Клиренс <span id="amount-five"></span></h2>
                            <div id="slider-range-five" class="someslider">

                                <form method="post">
                                    <input name="clirens-from" type="hidden" id="amount1">
                                    <input name="clirens-to" type="hidden" id="amount2">
                                </form>
                            </div>
                            <h2>Тип тормозов</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="drum-brakes" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Дисковые</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="disk-brakes" id="checkbox5" type="checkbox" value="Citroen">
                                </label>
                                <span>Барабанные</span>
                            </div>
                            <h2>Дифференциал</h2>
                            <!-- не учитывается -->
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chfalsex5" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                        </div>
                        <h1>Движение</h1>
                        <div>
                            <h2>Усилитель руля</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="no-power-steering" id="checkbox5" type="checkbox" value="Отсутствует">
                                </label>
                                <span>Отсутствует</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="gidro-power-steering" id="checkbox5" type="checkbox" value="Гидромеханический">
                                </label>
                                <span>Гидромеханический</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input name="electro-power-steering" id="checkbox5" type="checkbox" value="Электромеханическй">
                                </label>
                                <span>Электромеханическй</span>
                            </div>
                            <h2>Круиз-контроль</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="cruise-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="cruise-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Датчик положения в полосе</h2>
                            <!-- не учитывается -->
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Старт-Стоп</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="start-stop-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="start-stop-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>CitySafety</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="CitySafety-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="CitySafety-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Ночное видение</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="night-vision-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="night-vision-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Launch Control</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="launch-control-true" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chfalsex5" name="launch-control-false" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                        </div>
                        <h1>Комфорт</h1>
                        <div>
                            <h2>Климатическая система</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="conditioner" type="radio" value="Есть">
                                </label>
                                <span>Кондиционер</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chfalsex5" name="climat-system" type="radio" value="Нет">
                                </label>
                                <span>Климат-контроль</span>
                            </div>
                            <h2>Теплозащитное остекление</h2>
                            <div>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chtruex5" name="heat-insulating-glass-true" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox5" class="srch-box-chboxlabel">
                                    <input id="chfalsex5" name="heat-insulating-glass-false" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Парктроник</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="parktronic-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="parktronic-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Датчик света</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="light-sensor-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="light-sensor-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Датчик дождя</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="rain-sensor-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="rain-sensor-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Обогрев зеркал заднего вида</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-mirrors-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-mirrors-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Омыватель фар</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="headlight-washer-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="headlight-washer-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Электропривод зеркал</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="electric-mirrors-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="electric-mirrors-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Обогрев рулевого колеса</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-steering-wheel-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-steering-wheel-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Обогрев сидений</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-seats-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="heated-seats-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                            <h2>Система автоматической парковки</h2>
                            <div>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="automatic-parking-system-true" id="checkbox3" type="radio" value="Есть">
                                </label>
                                <span>Есть</span>
                                <label for="checkbox3" class="srch-box-chboxlabel">
                                    <input name="automatic-parking-system-false" id="checkbox3" type="radio" value="Нет">
                                </label>
                                <span>Нет</span>
                            </div>
                        </div>
                    </aside>
                </form>
                ​
            </div>
        </div>
    </div>
</section>

</#macro>