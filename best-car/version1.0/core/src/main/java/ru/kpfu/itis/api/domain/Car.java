package ru.kpfu.itis.api.domain;

import ru.kpfu.itis.api.Variables.*;

import javax.persistence.*;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Date;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Entity
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "carId")
    private List<Equipment> equipments;

    @ManyToOne(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY)
    private Compare compare;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "car")
    private List<Review> reviews;

    private String brand;
    private String model;
    private String generation;
    private String conditions;

    @OneToOne
    @JoinColumn(name = "car_id")
    private Car earlierGeneration;
    private Date startOfProduction;
    private Date endOfProduction;

    private String bodyType;


    private Double popularity;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "carId")
    private List<Image> imagesURL;


    //getters and setters

    public Car(){}

    public Car(List<String> imagesURL,String brand, String model, String generation,String condition,Date startOfProduction,Date endOfProduction, String bodyType){
        List<Image> images=new ArrayList<Image>();
        for(String s: imagesURL){
            images.add(new Image(this,s));
        }
        this.imagesURL=images;
        this.conditions=condition;
        this.brand=brand;
        this.model=model;
        this.generation=generation;
        this.startOfProduction=startOfProduction;
        this.endOfProduction=endOfProduction;
        this.bodyType=bodyType;
    }

    public Double getPopularity() {
        return popularity;
    }

    public void setPopularity(Double popularity) {
        this.popularity = popularity;
    }

    public void increasePopularityByFavourite(){
        if(popularity!=null){
            popularity+=2.0;
        }else{
            popularity=2.0;
        }
    }

    public void increasePopularityByReview(){
        if(popularity!=null){
            popularity+=1.0;
        }else{
            popularity=1.0;
        }
    }

    public void increasePopularityByComparison(){
        if(popularity!=null){
            popularity+=0.5;
        }else{
            popularity=0.5;
        }
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public List<Review> getReviews() {

        return reviews;
    }

    public List<Image> getImagesURL() {
        return imagesURL;
    }

    public void setImagesURL(List<Image> imagesURL) {
        this.imagesURL = imagesURL;
    }

    public double getMinimalPrice(){
        List<Equipment> equipments=getEquipments();
        Double price=equipments.get(0).getPrice();
        for(Equipment e: equipments){
            if(e.getPrice()<price){
                price=e.getPrice();
            }
        }
        return price;
    }


    //TODO if it is right way to code
    //TODO сделать метод выбора наилучшего варианта для главной страницы
    public String getTransmission(){
        List<Equipment> equipments=getEquipments();
        String transmission = "";
        if(getEquipments()!=null) {
            List<String> results = new ArrayList<String>();
            for (Equipment e : equipments) {
                if (!results.contains(e.getTransmission())) {
                    results.add(e.getTransmission());
                }
            }
            for (String str : results) {
                transmission += str + "/";
            }
            transmission=transmission.substring(0,transmission.length()-1);
        }else{
            //TODO LOGGING here
            int a=1+1;
        }
        return transmission;

    }
    public String getDriveUnit(){
        List<Equipment> equipments=getEquipments();
        String drive = "";
        if(getEquipments()!=null) {
            List<String> results = new ArrayList<String>();
            for (Equipment e : equipments) {
                if (!results.contains(e.getDriveUnit())) {
                    results.add(e.getDriveUnit());
                }
            }
            for (String str : results) {
                drive += str + "/";
            }
            drive=drive.substring(0,drive.length()-1);
        }else{
            //TODO LOGGING here
            int a=1+1;
        }
        return drive;
    }

    public String getIndexImageUrl(){
        return imagesURL.get(0).getImageURL();
    }

    public String getCatalogImageUrl(){
        return imagesURL.get(1).getImageURL();
    }

    public void setReviews(List<Review> reviews) {
        this.reviews = reviews;
    }

    public String getBodyType() {
        return bodyType;
    }

    public void setBodyType(String bodyType) {
        this.bodyType = bodyType;
    }

    public List<Equipment> getEquipments() {
        return equipments;
    }

    public String getCondition() {
        return conditions;
    }

    public void setCondition(String condition) {
        this.conditions = condition;
    }

    public void setEquipments(List<Equipment> equipments) {
        this.equipments = equipments;
    }

    public Compare getCompare() {
        return compare;
    }

    public void setCompare(Compare compare) {
        this.compare = compare;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Car getEarlierGeneration() {
        return earlierGeneration;
    }

    public void setEarlierGeneration(Car earlierGeneration) {
        this.earlierGeneration = earlierGeneration;
    }

    public Date getEndOfProduction() {
        return endOfProduction;
    }

    public void setEndOfProduction(Date endOfProduction) {
        this.endOfProduction = endOfProduction;
    }

    public String getGeneration() {
        return generation;
    }

    public void setGeneration(String generation) {
        this.generation = generation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Date getStartOfProduction() {
        return startOfProduction;
    }

    public void setStartOfProduction(Date startOfProduction) {
        this.startOfProduction = startOfProduction;
    }
}
