package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Image;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iPictureRepository {

    void addImage(Image image);

    List<Image> getAllImages();

    void updateImages(Image image);

    Image getImageById(Long id);

    void deleteImage(Image image);

}
