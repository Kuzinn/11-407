package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Roles;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.hibernateImpl.RolesRepository;
import ru.kpfu.itis.api.repository.hibernateImpl.UserRepository;
import ru.kpfu.itis.api.repository.iRolesRepository;
import ru.kpfu.itis.api.repository.iUserRepository;

import javax.transaction.Transactional;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Service
public class UserService {
    @Autowired
    private iUserRepository userRepository;
    @Autowired
    private iRolesRepository rolesRepository;

    @Transactional
    public void addUser(User user){
        userRepository.addUser(user);
    }
    @Transactional
    public void deleteUser(User user){
        userRepository.deleteUser(user);
    }
    @Transactional
    public void updateUser(User user){
        userRepository.updateUsers(user);
    }
    @Transactional
    public User getUserById(Long id){
        return userRepository.getUserById(id);
    }
    @Transactional
    public List<User> getAllUserses(){
        return userRepository.getAllUsers();
    }
    @Transactional
    public User getUserByName(String vasya) {
        return userRepository.getUserByName(vasya);
    }

    @Transactional
    public void createNewUser(String username,String email,String pass,String roles){
        User olya=new User();
        olya.setUsername(username);
        olya.setPassword(pass);
        olya.setEmail(email);
        olya.setEnabled(false);
        Collection<Roles> authorities = new HashSet<Roles>();
        for (final String role : roles.split(",")) {
            if (role != null && !"".equals(role.trim())) {
                Roles grantedAuthority=new Roles();
                grantedAuthority.setAuthority(role.trim());
                grantedAuthority.setUser(olya);
                authorities.add(grantedAuthority);

            }
        }
        olya.setAuthorities(authorities);
        userRepository.addUser(olya);
        for (Roles role : authorities){
            rolesRepository.addRoles(role);
        }
    }
}
