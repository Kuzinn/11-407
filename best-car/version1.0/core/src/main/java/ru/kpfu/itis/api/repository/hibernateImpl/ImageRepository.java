package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Image;
import ru.kpfu.itis.api.repository.iPictureRepository;

import java.util.List;

/**
 * Created by kuzin on 16.03.2016.
 */
@Repository
@Transactional
public class ImageRepository implements iPictureRepository {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void addImage(Image image){ sessionFactory.getCurrentSession().save(image);}

    @Override
    public List<Image> getAllImages(){
        return sessionFactory.getCurrentSession().createCriteria(Image.class).list();
    }

    @Override
    public void updateImages(Image image) {
        sessionFactory.getCurrentSession().update(image);
    }

    @Override
    public Image getImageById(Long id) {
        return (Image)sessionFactory.getCurrentSession().load(Image.class, id);
    }

    @Override
    public void deleteImage(Image image) {
        sessionFactory.getCurrentSession().delete(image);
    }

}
