package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Car;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iCarRepository {

    void addCar(Car car) ;

    List<Car> getAllCars() ;

    void updateCars(Car car) ;

    Car getCarById(Long id) ;

    void deleteCar(Car car);

    List<Car> execCarSQL(String sql) ;

    List<Car> getCarsOrderedByDate(int count) ;

    List<Car> getCarsOrderedByPopularity(int count) ;

    List<Car> getCarByMainFilters(Double moneyFrom, Double moneyTo, List<String> conditions, List<String> gearBoxes,
                                         List<String> brands);

}



