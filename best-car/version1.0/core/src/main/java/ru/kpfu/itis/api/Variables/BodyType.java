package ru.kpfu.itis.api.Variables;

/**
 * Created by kuzin on 09.03.2016.
 */
public enum BodyType {
    SEDAN,
    HATCHBACK,
    COUPE,
    STATION_WAGONS,
    CROSSOVER,
    ROADSTER,
    MINIVAN,
    MULTIVAN,
    GRAND_TOURER,
    TAGRA,
    CONVERTIBLE,
    VAN,
    HARDTOP,
    LIFTBACK,
    PHAETON,
    SPIDER,
    SHOOTINGBRAKE,
    TRUCK,
    PICKUP;

    }
