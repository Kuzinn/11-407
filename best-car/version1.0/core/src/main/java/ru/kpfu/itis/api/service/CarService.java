package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.Equipment;
import ru.kpfu.itis.api.domain.Image;
import ru.kpfu.itis.api.repository.hibernateImpl.CarRepository;
import ru.kpfu.itis.api.repository.hibernateImpl.EquipmentRepository;
import ru.kpfu.itis.api.repository.hibernateImpl.ImageRepository;
import ru.kpfu.itis.api.repository.iCarRepository;
import ru.kpfu.itis.api.repository.iEquipmentRepository;
import ru.kpfu.itis.api.repository.iPictureRepository;


import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Service
public class CarService {


    @Autowired
    private iCarRepository carRepository;
    @Autowired
    private iEquipmentRepository equipmentRepository;
    @Autowired
    private iPictureRepository imageRepository;

    @Transactional
    public void addCar(Car car){
        carRepository.addCar(car);
    }
    @Transactional
    public void deleteCar(Car car){
        carRepository.deleteCar(car);
    }
    @Transactional
    public void updateCar(Car car){
        carRepository.updateCars(car);
    }
    @Transactional
    public Car getCarById(Long id){
        return carRepository.getCarById(id);
    }
    @Transactional
    public List<Car> getAllCarses(){
        return carRepository.getAllCars();
    }
    @Transactional
    public List<Car> execQuery(String sql){return carRepository.execCarSQL(sql);}
    @Transactional
    public void createCar(List<String> images, String brand, String model, String generation,String condition, Date startOfProduction,
                          Date endOfProduction, String bodyType, String nameOfEquip, Double price, String clearance,
                          String driveUnit, String transmission, String engineType, int enginePower, int torque, String brakes,
                          String acceleration, String fuelConsumption, String headlights, boolean abs, boolean esp,
                          boolean tractionControl, String airConditioning, boolean cruiseControl, String powerSteering,
                          boolean parktronic, boolean rainSensor, boolean lightSensor, boolean heatedMirrors,
                          boolean headlightWasher, boolean electricMirrors, boolean heatedSteeringWheel, boolean seatHeating,
                          String sunroof, int numberOfSeats, boolean centralLocking, String signaling,
                          String airBag, boolean startStop , String fuelTank , String ownWeight , String leatherSeats ,
                          String projector , String audioSystem , String electricSpeedometer , boolean automaticParkingSystem ,
                          boolean bodyColorMirrors , String ceiling , String dashboard , boolean heatInsulatingGlass ,
                          boolean adaptiveSuspension , boolean launchControl
                          ){
        Car car=new Car(images,brand, model,generation,condition,startOfProduction,endOfProduction,bodyType);
        for(String s: images){
            imageRepository.addImage(new Image(car,s));
        }
        carRepository.addCar(car);
        Equipment equipment=new Equipment(car, nameOfEquip, price, clearance, driveUnit, transmission,
                engineType, enginePower, torque, brakes, acceleration, fuelConsumption, headlights,abs,esp,
                tractionControl,airConditioning,cruiseControl,powerSteering, parktronic,rainSensor,lightSensor,
                heatedMirrors, headlightWasher,electricMirrors,heatedSteeringWheel,seatHeating, sunroof,numberOfSeats,
                centralLocking,signaling,airBag,startStop ,fuelTank ,ownWeight ,leatherSeats ,projector ,audioSystem ,
                electricSpeedometer ,automaticParkingSystem , bodyColorMirrors ,ceiling ,dashboard ,heatInsulatingGlass ,
                adaptiveSuspension ,launchControl
        );
        equipmentRepository.addEquipment(equipment);
        List<Equipment> equipments=new ArrayList<Equipment>();
        equipments.add(equipment);
        car.setEquipments(equipments);
        carRepository.updateCars(car);
    }

    @Transactional
    public List<String> getAllBrands(){
        List<String> allBrands=new ArrayList<String>();
        List<Car> cars=carRepository.getAllCars();
        for(Car c: cars){
            if(!allBrands.contains(c.getBrand())){
                allBrands.add(c.getBrand());
            }
        }
        return allBrands;
    }


    public List<Car> getPopularCars(int count){return carRepository.getCarsOrderedByPopularity(count);}


    public List<Car> getNewCars(int count){return carRepository.getCarsOrderedByDate(count);}

    public List<Car> getCarByMainFilters(Double moneyFrom,Double moneyTo,List<String> conditions,List<String> gearBoxes,
                                         List<String> brands){

        return carRepository.getCarByMainFilters(moneyFrom,moneyTo,conditions,gearBoxes,brands);

    }

}