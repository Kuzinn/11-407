package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Equipment;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.iEquipmentRepository;

import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Repository
@Transactional
public class EquipmentRepository implements iEquipmentRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addEquipment(Equipment equipment){ sessionFactory.getCurrentSession().save(equipment);}


    /**
     *
     * @param values коллекция названий строк в таблице
     */
    @Override
    public void addEquipmentByValues(List<String> values) {
        StringBuilder sb=new StringBuilder();
        for (int i = 0; i < values.size(); i++) {
            sb.append(values.get(i));
            sb.append(",");
        }
        String vals=sb.toString().substring(0,sb.toString().length()-1);

        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(
                    "INSERT INTO equipment (carId_id,name,price,clearance,driveUnit," +
                            "transmission,engineType,enginePower,torque,brakes,acceleration,fuelConsumption," +
                            "headlights,abs,esp,airConditioning,cruiseControl,powerSteering," +
                            "parktronic,rainSensor,lightSensor,heatedMirrors,headlightWasher," +
                            "electricMirrors,heatedSteeringWheel,seatHeating,sunroof,numberOfSeats,centralLocking," +
                            "signaling,airBag,startStop,fuelTank,ownWeight,leatherSeats,projector,audioSystem," +
                            "electricSpeedometer,automaticParkingSystem,bodyColorMirrors," +
                            "ceiling,dashboard,heatInsulatingGlass,adaptiveSuspension,launchControl,tractionControl) VALUES ("+vals+")"
            );
            sqlQuery.executeUpdate();
        }catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Equipment> getAllEquipments(){
        return sessionFactory.getCurrentSession().createCriteria(Equipment.class).list();
    }

    @Override
    public void updateEquipments(Equipment equipment) {
        sessionFactory.getCurrentSession().update(equipment);
    }

    @Override
    public Equipment getEquipmentById(Long id) {
        return (Equipment)sessionFactory.getCurrentSession().load(Equipment.class, id);
    }

    @Override
    public void deleteEquipment(Equipment equipment) {
        sessionFactory.getCurrentSession().delete(equipment);
    }
}
