package ru.kpfu.itis.api.repository;


import ru.kpfu.itis.api.domain.Compare;
import ru.kpfu.itis.api.domain.User;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iCompareRepository {

    void addCompare(Compare compare);

    List<Compare> getAllCompares();

    void updateCompares(Compare compare);

    Compare getCompareById(Long id) ;

    void deleteCompare(Compare compare);

    List<Compare> getAllComparesFor(User user);
}
