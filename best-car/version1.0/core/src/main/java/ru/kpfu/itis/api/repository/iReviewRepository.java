package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Review;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iReviewRepository {

    void addReview(Review review);

    List<Review> getAllReviews();

    void updateReviews(Review review);

    Review getReviewById(Long id);

    void deleteReview(Review review);

    List<Review> getReviewsByCarId(Long id);

}
