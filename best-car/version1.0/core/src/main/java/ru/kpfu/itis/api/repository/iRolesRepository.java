package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Roles;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iRolesRepository {

    void addRoles(Roles role);

    List<Roles> getAllRoless();

    void updateRoless(Roles role);

    Roles getRolesById(Long id);

    void deleteRoles(Roles role);

}
