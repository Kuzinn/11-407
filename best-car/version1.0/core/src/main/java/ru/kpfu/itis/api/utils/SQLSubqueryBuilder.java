package ru.kpfu.itis.api.utils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 18.03.2016.
 */
public class SQLSubqueryBuilder {
    //( car.brand = BMW ) <-|
    public static String buildParamsSubquery(List<String> params,String tableName,String tableRowName,String comparing){
        StringBuilder sb=new StringBuilder("");
        if(params.size()>0) {
            sb.append(" AND ");
            sb.append("( ");
            for (String br : params) {
                sb.append(tableName);
                sb.append(".");
                sb.append(tableRowName);
                sb.append(comparing);
                sb.append("'");
                sb.append(br);
                sb.append("'");

                if (params.indexOf(br) == params.size()-1) {
                    sb.append(" ) ");
                } else {
                    sb.append(" or ");
                }
                System.out.println(params.size());
                System.out.println(params.indexOf(br));
            }
        }
        return sb.toString();
    }

    public static void main(String[] args) {
        String mf="\u20BD ";
        String mt=null;

        mf= mf.length()>2?mf.substring(2).replace(" ",""):"";
        mt= mt.length()>2?mt.substring(2).replace(" ",""):"";
        System.out.println(mf);
        System.out.println(mt);

    }
}
