package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Equipment;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iEquipmentRepository {

    void addEquipment(Equipment equipment);

    void addEquipmentByValues(List<String> values);

    List<Equipment> getAllEquipments();

    void updateEquipments(Equipment equipment);

    Equipment getEquipmentById(Long id);

    void deleteEquipment(Equipment equipment);

}
