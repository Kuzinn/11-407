package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.api.repository.hibernateImpl.ReviewRepository;
import ru.kpfu.itis.api.repository.iReviewRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kuzin on 16.03.2016.
 */
@Service
public class ReviewService {
    @Autowired
    private iReviewRepository reviewRepository;

    @Autowired
    private CarService carService;

    @Transactional
    public void addReview(Review review){
        reviewRepository.addReview(review);
    }
    @Transactional
    public void deleteReview(Review review){
        reviewRepository.deleteReview(review);
    }
    @Transactional
    public void updateReview(Review review){
        reviewRepository.updateReviews(review);
    }
    @Transactional
    public Review getReviewById(Long id){
        return reviewRepository.getReviewById(id);
    }
    @Transactional
    public List<Review> getAllReviewses(){
        return reviewRepository.getAllReviews();
    }

    public List<Review> getReviewsByCarId(Long id){return reviewRepository.getReviewsByCarId(id);}

    @Transactional
    public Double getAvgRatingByCarID(Long id){
        List<Review> reviews=reviewRepository.getReviewsByCarId(id);
        Double ratingSumm=0.0;
        for(Review r: reviews){
            ratingSumm+=r.getRating();
        }
        Double rating = ratingSumm / reviews.size();
        if (rating%1 <= 0.2){
            rating = Math.floor(rating);
        }else if(rating%1 >= 0.8) {
            rating = Math.ceil(rating);
        }
        return rating;
    }
}
