package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.User;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iUserRepository {

    void addUser(User user);

    List<User> getAllUsers();

    User getUserByName(String name);

    void updateUsers(User user);

    User getUserById(Long id);

    void deleteUser(User user);

}
