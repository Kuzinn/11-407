package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.repository.iCarRepository;
import ru.kpfu.itis.api.utils.SQLSubqueryBuilder;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Repository
@Transactional
public class CarRepository implements iCarRepository {

    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void addCar(Car car){ sessionFactory.getCurrentSession().save(car);}

    @Override
    public List<Car> getAllCars(){
        return sessionFactory.getCurrentSession().createCriteria(Car.class).list();
    }

    @Override
    public void updateCars(Car car) {
        sessionFactory.getCurrentSession().update(car);
    }

    @Override
    public Car getCarById(Long id) {
        return (Car)sessionFactory.getCurrentSession().load(Car.class, id);
    }

    @Override
    public void deleteCar(Car car) {
        sessionFactory.getCurrentSession().delete(car);
    }

    @Override
    public List<Car> execCarSQL(String sql){
        List<Car> cars=new ArrayList<Car>();
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery(sql)
                    .addEntity(Car.class);
            cars = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in execCarSQL(String sql): " + e.getMessage());
        }
        return cars;
    }

    @Override
    public List<Car> getCarsOrderedByDate(int count){
        List<Car> cars=null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery("select * from car order by startOfProduction")
                    .addEntity(Car.class);
            cars = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getCarsOrderedByDate(int count): " + e.getMessage());
        }
        List<Car> result=cars.subList(0,count);
        return result;
    }

    @Override
    public List<Car> getCarsOrderedByPopularity(int count){
        List<Car> cars=null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery("select * from car order by popularity DESC")
                    .addEntity(Car.class);
            cars = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getCarsOrderedByPopularity(int count): " + e.getMessage());
        }
        List<Car> result=cars.subList(0,count);
        return result;
    }

    @Override
    public List<Car> getCarByMainFilters(Double moneyFrom,Double moneyTo,List<String> conditions,List<String> gearBoxes,
                                         List<String> brands){
        List<Car> cars=null;
        SQLSubqueryBuilder builder=new SQLSubqueryBuilder();
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession()
                    .createSQLQuery("select * from car,equipment where car.id=equipment.carId_id and ( equipment.price <"+ moneyTo.intValue()
                            + " and " + "equipment.price > "+moneyFrom.intValue() +" )"+
                            builder.buildParamsSubquery(conditions,"car","conditions","=")+
                            builder.buildParamsSubquery(gearBoxes,"equipment","transmission"," like ")+
                            builder.buildParamsSubquery(brands,"car","brand","=")

                    )
                    .addEntity(Car.class);
            cars = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getCarByMainFilters(): " + e.getMessage());
        }

        return cars;
    }
}
