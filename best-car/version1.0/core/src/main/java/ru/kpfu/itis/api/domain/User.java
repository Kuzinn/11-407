package ru.kpfu.itis.api.domain;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import ru.kpfu.itis.api.Variables.BodyType;
import ru.kpfu.itis.api.Variables.Transmission;

import javax.annotation.Generated;
import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by kuzin on 07.03.2016.
 */
@Entity
public class User implements UserDetails{

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String username;
    private String password;
    private boolean enabled;
    private String email;

    @OneToMany(cascade = CascadeType.REFRESH,
            fetch = FetchType.LAZY,
            mappedBy = "user")
    private Collection<Roles> authorities;

    public User(){}

//    public User(String username, String password, String roles) {
//        super();
//        this.username = username;
//        this.password = password;
//        this.setAuthorities(roles);
//    }


    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setAuthorities(Collection<Roles> authorities) {
        this.authorities = authorities;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
    //    public void setAuthorities(String roles) {
//        this.authorities = new HashSet<Roles>();
//        for (final String role : roles.split(",")) {
//            if (role != null && !"".equals(role.trim())) {
//                Roles grantedAuthority=new Roles();
//                grantedAuthority.setAuthority(role.trim());
////                GrantedAuthority grandAuthority2 = new GrantedAuthority() {
////                    private static final long serialVersionUID = 3958183417696804555L;
////
////                    public String getAuthority() {
////                        return role.trim();
////                    }
////                };
//                this.authorities.add(grantedAuthority);
//            }
//        }
//    }


    public Long getId() {
        return id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<? extends GrantedAuthority> getAuthorities() {
        return this.authorities;
    }

    public String getPassword() {
        return password;
    }

    public String getUsername() {
        return username;
    }

    public boolean isAccountNonExpired() {
        return true;
    }

    public boolean isAccountNonLocked() {
        return true;
    }

    public boolean isCredentialsNonExpired() {
        return true;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
