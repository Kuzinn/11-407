package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Favourites;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.hibernateImpl.FavouritesRepository;
import ru.kpfu.itis.api.repository.iFavouritesRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Service
public class FavouritesService {
    @Autowired
    private iFavouritesRepository favouritesRepository;

    @Transactional
    public void addFavourites(Favourites favourites){
        favouritesRepository.addFavourites(favourites);
    }
    @Transactional
    public void deleteFavourites(Favourites favourites){
        favouritesRepository.deleteFavourites(favourites);
    }
    @Transactional
    public void updateFavourites(Favourites favourites){
        favouritesRepository.updateFavouritess(favourites);
    }
    @Transactional
    public Favourites getFavouritesById(Long id){
        return favouritesRepository.getFavouritesById(id);
    }
    @Transactional
    public List<Favourites> getAllFavouritesses(){
        return favouritesRepository.getAllFavourites();
    }

    @Transactional
    public List<Favourites> getAllFavouritesFor(User user){ return favouritesRepository.getAllFavouritesFor(user);}
}
