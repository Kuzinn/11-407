package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Roles;
import ru.kpfu.itis.api.repository.iRolesRepository;

import java.util.List;

/**
 * Created by kuzin on 13.03.2016.
 */
@Repository
@Transactional
public class RolesRepository implements iRolesRepository {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void addRoles(Roles role){ sessionFactory.getCurrentSession().save(role);}

    @Override
    public List<Roles> getAllRoless(){
        return sessionFactory.getCurrentSession().createCriteria(Roles.class).list();
    }

    @Override
    public void updateRoless(Roles role) {
        sessionFactory.getCurrentSession().update(role);
    }

    @Override
    public Roles getRolesById(Long id) {
        return (Roles)sessionFactory.getCurrentSession().load(Roles.class, id);
    }

    @Override
    public void deleteRoles(Roles role) {
        sessionFactory.getCurrentSession().delete(role);
    }

}
