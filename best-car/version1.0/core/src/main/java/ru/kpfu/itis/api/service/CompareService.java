package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Compare;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.hibernateImpl.CompareRepository;
import ru.kpfu.itis.api.repository.iCompareRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Service
public class CompareService {
    @Autowired
    private iCompareRepository compareRepository;

    @Transactional
    public void addCompare(Compare compare){
        compareRepository.addCompare(compare);
    }
    @Transactional
    public void deleteCompare(Compare compare){
        compareRepository.deleteCompare(compare);
    }
    @Transactional
    public void updateCompare(Compare compare){
        compareRepository.updateCompares(compare);
    }
    @Transactional
    public Compare getCompareById(Long id){
        return compareRepository.getCompareById(id);
    }
    @Transactional
    public List<Compare> getAllCompareses(){
        return compareRepository.getAllCompares();
    }
    @Transactional
    public List<Compare> getAllComparesesFor(User user){return compareRepository.getAllComparesFor(user);}
}
