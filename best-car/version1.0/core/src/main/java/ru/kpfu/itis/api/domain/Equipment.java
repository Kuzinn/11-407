package ru.kpfu.itis.api.domain;

import ru.kpfu.itis.api.Variables.*;

import javax.persistence.*;

/**
 * Created by kuzin on 09.03.2016.
 */
@Entity
public class Equipment {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne(cascade = {CascadeType.REFRESH},
            fetch = FetchType.LAZY)
    private Car carId;
    private String name;
    private Double price;

    private String clearance;
    private String driveUnit;//привод
    private String transmission;
    private String engineType;
    private int enginePower;
    private int torque;
    private String brakes;
    private String acceleration;
    private String fuelConsumption;
    private String headlights;
    private boolean abs;
    private boolean esp;
    private boolean tractionControl;

    private String airConditioning;
    private boolean cruiseControl;
    private String powerSteering;
    private boolean parktronic;
    private boolean rainSensor;
    private boolean lightSensor;
    private boolean heatedMirrors;
    private boolean headlightWasher;
    private boolean electricMirrors;
    private boolean heatedSteeringWheel;
    private boolean seatHeating;
    private String sunroof;
    private int numberOfSeats;
    private boolean centralLocking;
    private String signaling;
    private String airBag;
    private boolean startStop;
    private String fuelTank;
    private String ownWeight;
    private String leatherSeats;
    private String projector;
    private String audioSystem;
    private String electricSpeedometer;
    private boolean automaticParkingSystem;
    private boolean bodyColorMirrors;
    private String ceiling;
    private String dashboard;
    private boolean heatInsulatingGlass;
    private boolean adaptiveSuspension;
    private boolean launchControl;


    //getters and setters

    public Equipment(){}

    public Equipment(Car carId,String nameOfEquip, Double price, String clearance,
                     String driveUnit, String transmission, String engineType,int enginePower,int torque, String brakes,
                     String acceleration, String fuelConsumption, String headlights, boolean abs, boolean esp,
                     boolean tractionControl, String airConditioning, boolean cruiseControl, String powerSteering,
                     boolean parktronic, boolean rainSensor, boolean lightSensor, boolean heatedMirrors,
                     boolean headlightWasher, boolean electricMirrors, boolean heatedSteeringWheel, boolean seatHeating,
                     String sunroof, int numberOfSeats, boolean centralLocking, String signaling,
                     String airBag,boolean startStop ,String fuelTank ,String ownWeight ,String leatherSeats ,
                     String projector ,String audioSystem ,String electricSpeedometer ,boolean automaticParkingSystem ,
                     boolean bodyColorMirrors ,String ceiling ,String dashboard ,boolean heatInsulatingGlass ,
                     boolean adaptiveSuspension ,boolean launchControl){

        this.carId=carId;
        this.name=nameOfEquip;
        this.price=price;
        this.clearance=clearance;
        this.driveUnit=driveUnit;
        this.transmission=transmission;
        this.engineType=engineType;
        this.brakes=brakes;
        this.acceleration=acceleration;
        this.fuelConsumption=fuelConsumption;
        this.headlights=headlights;
        this.abs=abs;
        this.enginePower=enginePower;
        this.esp=esp;
        this.tractionControl=tractionControl;
        this.airConditioning=airConditioning;
        this.cruiseControl=cruiseControl;
        this.powerSteering=powerSteering;
        this.parktronic=parktronic;
        this.rainSensor=rainSensor;
        this.lightSensor=lightSensor;
        this.heatedMirrors=heatedMirrors;
        this.headlightWasher=headlightWasher;
        this.electricMirrors=electricMirrors;
        this.heatedSteeringWheel=heatedSteeringWheel;
        this.seatHeating=seatHeating;
        this.sunroof=sunroof;
        this.numberOfSeats=numberOfSeats;
        this.centralLocking=centralLocking;
        this.signaling=signaling;
        this.airBag=airBag;
        this.startStop=startStop;
        this.fuelTank=fuelTank;
        this.ownWeight=ownWeight;
        this.leatherSeats=leatherSeats;
        this.projector=projector;
        this.audioSystem=audioSystem;
        this.electricSpeedometer=electricSpeedometer;
        this.automaticParkingSystem=automaticParkingSystem;
        this.bodyColorMirrors=bodyColorMirrors;
        this.ceiling=ceiling;
        this.dashboard=dashboard;
        this.heatInsulatingGlass=heatInsulatingGlass;
        this.adaptiveSuspension=adaptiveSuspension;
        this.launchControl=launchControl;
        this.torque=torque;

    }

    public boolean isAbs() {
        return abs;
    }

    public void setAbs(boolean abs) {
        this.abs = abs;
    }

    public String getAcceleration() {
        return acceleration;
    }

    public int getEnginePower() {
        return enginePower;
    }

    public void setEnginePower(int enginePower) {
        this.enginePower = enginePower;
    }

    public void setAcceleration(String acceleration) {
        this.acceleration = acceleration;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getAirConditioning() {
        return airConditioning;
    }

    public void setAirConditioning(String airConditioning) {
        this.airConditioning = airConditioning;
    }

    public String getBrakes() {
        return brakes;
    }

    public void setBrakes(String brakes) {
        this.brakes = brakes;
    }

    public Car getCarId() {
        return carId;
    }

    public void setCarId(Car carId) {
        this.carId = carId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean isAdaptiveSuspension() {
        return adaptiveSuspension;
    }

    public void setAdaptiveSuspension(boolean adaptiveSuspension) {
        this.adaptiveSuspension = adaptiveSuspension;
    }

    public String getAirBag() {
        return airBag;
    }

    public void setAirBag(String airBag) {
        this.airBag = airBag;
    }

    public String getAudioSystem() {
        return audioSystem;
    }

    public void setAudioSystem(String audioSystem) {
        this.audioSystem = audioSystem;
    }

    public boolean isAutomaticParkingSystem() {
        return automaticParkingSystem;
    }

    public void setAutomaticParkingSystem(boolean automaticParkingSystem) {
        this.automaticParkingSystem = automaticParkingSystem;
    }

    public boolean isBodyColorMirrors() {
        return bodyColorMirrors;
    }

    public void setBodyColorMirrors(boolean bodyColorMirrors) {
        this.bodyColorMirrors = bodyColorMirrors;
    }

    public String getCeiling() {
        return ceiling;
    }

    public void setCeiling(String ceiling) {
        this.ceiling = ceiling;
    }

    public String getDashboard() {
        return dashboard;
    }

    public void setDashboard(String dashboard) {
        this.dashboard = dashboard;
    }

    public String getElectricSpeedometer() {
        return electricSpeedometer;
    }

    public void setElectricSpeedometer(String electricSpeedometer) {
        this.electricSpeedometer = electricSpeedometer;
    }

    public String getFuelTank() {
        return fuelTank;
    }

    public void setFuelTank(String fuelTank) {
        this.fuelTank = fuelTank;
    }

    public boolean isHeatInsulatingGlass() {
        return heatInsulatingGlass;
    }

    public void setHeatInsulatingGlass(boolean heatInsulatingGlass) {
        this.heatInsulatingGlass = heatInsulatingGlass;
    }

    public boolean isLaunchControl() {
        return launchControl;
    }

    public void setLaunchControl(boolean launchControl) {
        this.launchControl = launchControl;
    }

    public String getLeatherSeats() {
        return leatherSeats;
    }

    public void setLeatherSeats(String leatherSeats) {
        this.leatherSeats = leatherSeats;
    }

    public String getOwnWeight() {
        return ownWeight;
    }

    public void setOwnWeight(String ownWeight) {
        this.ownWeight = ownWeight;
    }

    public String getProjector() {
        return projector;
    }

    public void setProjector(String projector) {
        this.projector = projector;
    }

    public boolean isStartStop() {
        return startStop;
    }

    public void setStartStop(boolean startStop) {
        this.startStop = startStop;
    }

    public int getTorque() {
        return torque;
    }

    public void setTorque(int torque) {
        this.torque = torque;
    }

    public boolean isCentralLocking() {
        return centralLocking;
    }

    public void setCentralLocking(boolean centralLocking) {
        this.centralLocking = centralLocking;
    }

    public String getClearance() {
        return clearance;
    }

    public void setClearance(String clearance) {
        this.clearance = clearance;
    }

    public boolean isCruiseControl() {
        return cruiseControl;
    }

    public void setCruiseControl(boolean cruiseControl) {
        this.cruiseControl = cruiseControl;
    }

    public String getDriveUnit() {
        return driveUnit;
    }

    public void setDriveUnit(String driveUnit) {
        this.driveUnit = driveUnit;
    }

    public boolean isElectricMirrors() {
        return electricMirrors;
    }

    public void setElectricMirrors(boolean electricMirrors) {
        this.electricMirrors = electricMirrors;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public boolean isEsp() {
        return esp;
    }

    public void setEsp(boolean esp) {
        this.esp = esp;
    }

    public String getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(String fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }

    public String getHeadlights() {
        return headlights;
    }

    public void setHeadlights(String headlights) {
        this.headlights = headlights;
    }

    public boolean isHeadlightWasher() {
        return headlightWasher;
    }

    public void setHeadlightWasher(boolean headlightWasher) {
        this.headlightWasher = headlightWasher;
    }

    public boolean isHeatedMirrors() {
        return heatedMirrors;
    }

    public void setHeatedMirrors(boolean heatedMirrors) {
        this.heatedMirrors = heatedMirrors;
    }

    public boolean isHeatedSteeringWheel() {
        return heatedSteeringWheel;
    }

    public void setHeatedSteeringWheel(boolean heatedSteeringWheel) {
        this.heatedSteeringWheel = heatedSteeringWheel;
    }

    public boolean isLightSensor() {
        return lightSensor;
    }

    public void setLightSensor(boolean lightSensor) {
        this.lightSensor = lightSensor;
    }

    public int getNumberOfSeats() {
        return numberOfSeats;
    }

    public void setNumberOfSeats(int numberOfSeats) {
        this.numberOfSeats = numberOfSeats;
    }

    public boolean isParktronic() {
        return parktronic;
    }

    public void setParktronic(boolean parktronic) {
        this.parktronic = parktronic;
    }

    public String getPowerSteering() {
        return powerSteering;
    }

    public void setPowerSteering(String powerSteering) {
        this.powerSteering = powerSteering;
    }

    public boolean isRainSensor() {
        return rainSensor;
    }

    public void setRainSensor(boolean rainSensor) {
        this.rainSensor = rainSensor;
    }

    public boolean isSeatHeating() {
        return seatHeating;
    }

    public void setSeatHeating(boolean seatHeating) {
        this.seatHeating = seatHeating;
    }

    public String getSignaling() {
        return signaling;
    }

    public void setSignaling(String signaling) {
        this.signaling = signaling;
    }

    public String getSunroof() {
        return sunroof;
    }

    public void setSunroof(String sunroof) {
        this.sunroof = sunroof;
    }

    public boolean isTractionControl() {
        return tractionControl;
    }

    public void setTractionControl(boolean tractionControl) {
        this.tractionControl = tractionControl;
    }

    public String getTransmission() {
        return transmission;
    }

    public void setTransmission(String transmission) {
        this.transmission = transmission;
    }
}
