package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Review;
import ru.kpfu.itis.api.repository.iReviewRepository;

import java.util.List;

/**
 * Created by kuzin on 16.03.2016.
 */
@Repository
@Transactional
public class ReviewRepository implements iReviewRepository {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void addReview(Review review){ sessionFactory.getCurrentSession().save(review);}

    @Override
    public List<Review> getAllReviews(){
        return sessionFactory.getCurrentSession().createCriteria(Review.class).list();
    }

    @Override
    public void updateReviews(Review review) {
        sessionFactory.getCurrentSession().update(review);
    }

    @Override
    public Review getReviewById(Long id) {
        return (Review)sessionFactory.getCurrentSession().load(Review.class, id);
    }

    @Override
    public void deleteReview(Review review) {
        sessionFactory.getCurrentSession().delete(review);
    }

    @Override
    public List<Review> getReviewsByCarId(Long id){
        List<Review> reviews=null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession()
                    .createSQLQuery("select * from review where car_id="+id)
                    .addEntity(Review.class);
            reviews = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getCarByMainFilters(): " + e.getMessage());
        }
        return reviews;
    }

}
