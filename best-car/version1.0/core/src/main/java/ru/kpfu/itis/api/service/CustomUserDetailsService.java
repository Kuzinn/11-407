package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.kpfu.itis.api.domain.User;

/**
 * Created by kuzin on 12.03.2016.
 */

public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserService userService;
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        User user=userService.getUserByName(s);
        if(user==null){
            throw new UsernameNotFoundException("User with name " + s + " not found");
        }
        return userService.getUserByName(s);
    }
}
