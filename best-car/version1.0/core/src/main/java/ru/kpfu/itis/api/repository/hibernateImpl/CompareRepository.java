package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Compare;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.iCompareRepository;

import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Repository
@Transactional
public class CompareRepository implements iCompareRepository {
    @Autowired
    protected SessionFactory sessionFactory;

    @Override
    public void addCompare(Compare compare){ sessionFactory.getCurrentSession().save(compare);}

    @Override
    public List<Compare> getAllCompares(){
        return sessionFactory.getCurrentSession().createCriteria(Compare.class).list();
    }

    @Override
    public void updateCompares(Compare compare) {
        sessionFactory.getCurrentSession().update(compare);
    }

    @Override
    public Compare getCompareById(Long id) {
        return (Compare)sessionFactory.getCurrentSession().load(Compare.class, id);
    }

    @Override
    public void deleteCompare(Compare compare) {
        sessionFactory.getCurrentSession().delete(compare);
    }

    @Override
    public List<Compare> getAllComparesFor(User user){
        List<Compare> coms=null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession()
                    .createSQLQuery("select * from compare where user_id="+user.getId())
                    .addEntity(Compare.class);
            coms = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getAllComparesFor(): " + e.getMessage());
        }
        return coms;
    }
}
