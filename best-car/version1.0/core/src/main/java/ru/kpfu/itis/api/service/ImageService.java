package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Image;
import ru.kpfu.itis.api.repository.hibernateImpl.ImageRepository;
import ru.kpfu.itis.api.repository.iPictureRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kuzin on 16.03.2016.
 */
@Service
public class ImageService {
    @Autowired
    private iPictureRepository imageRepository;

    @Transactional
    public void addImage(Image image){
        imageRepository.addImage(image);
    }
    @Transactional
    public void deleteImage(Image image){
        imageRepository.deleteImage(image);
    }
    @Transactional
    public void updateImage(Image image){
        imageRepository.updateImages(image);
    }
    @Transactional
    public Image getImageById(Long id){
        return imageRepository.getImageById(id);
    }
    @Transactional
    public List<Image> getAllImageses(){
        return imageRepository.getAllImages();
    }

}
