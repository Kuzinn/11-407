package ru.kpfu.itis.api.domain.interfaces;

import ru.kpfu.itis.api.domain.Car;
import ru.kpfu.itis.api.domain.User;

import javax.persistence.*;

/**
 * Created by kuzin on 01.04.2016.
 */
public abstract class WishList {

    public Long id ;
    public User user ;
    public Car car ;

    public Car getCar(){return car;}

    public void setCar(Car car){this.car=car;}

    public Long getId(){return id;}

    public void setId(Long id){this.id=id;}

    public User getUser(){return user;}

    public void setUser(User user){this.user=user;}
}
