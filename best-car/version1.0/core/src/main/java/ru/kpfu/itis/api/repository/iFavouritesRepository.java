package ru.kpfu.itis.api.repository;

import ru.kpfu.itis.api.domain.Favourites;
import ru.kpfu.itis.api.domain.User;

import java.util.List;

/**
 * Created by kuzin on 11.04.2016.
 */
public interface iFavouritesRepository {

    void addFavourites(Favourites favourites);

    List<Favourites> getAllFavourites();

    void updateFavouritess(Favourites favourites);

    Favourites getFavouritesById(Long id) ;

    void deleteFavourites(Favourites favourites);

    List<Favourites> getAllFavouritesFor(User user);

}
