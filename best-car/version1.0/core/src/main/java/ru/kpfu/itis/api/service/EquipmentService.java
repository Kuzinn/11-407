package ru.kpfu.itis.api.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kpfu.itis.api.domain.Equipment;
import ru.kpfu.itis.api.repository.hibernateImpl.EquipmentRepository;
import ru.kpfu.itis.api.repository.iEquipmentRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Service
public class EquipmentService {
    @Autowired
    private iEquipmentRepository equipmentRepository;

    @Transactional
    public void addEquipmentByValue(List<String> values){equipmentRepository.addEquipmentByValues(values);}
    @Transactional
    public void addEquipment(Equipment equipment){
        equipmentRepository.addEquipment(equipment);
    }
    @Transactional
    public void deleteEquipment(Equipment equipment){
        equipmentRepository.deleteEquipment(equipment);
    }
    @Transactional
    public void updateEquipment(Equipment equipment){
        equipmentRepository.updateEquipments(equipment);
    }
    @Transactional
    public Equipment getEquipmentById(Long id){
        return equipmentRepository.getEquipmentById(id);
    }
    @Transactional
    public List<Equipment> getAllEquipmentses(){
        return equipmentRepository.getAllEquipments();
    }
}
