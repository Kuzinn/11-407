package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.iUserRepository;

import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Repository
@Transactional
public class UserRepository implements iUserRepository {
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addUser(User user){ sessionFactory.getCurrentSession().save(user);}

    @Override
    public List<User> getAllUsers(){

        return sessionFactory.getCurrentSession().createCriteria(User.class).list();
    }

    @Override
    public User getUserByName(String name){
        User petya = null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession().createSQLQuery("select * from user where username = :petya") // задаем SQL запрос
                    .addEntity(User.class);                  // указываем сущность ожидаемую в ответе
            Query query = sqlQuery.setString("petya", name);
            petya = (User) query.uniqueResult();          // получаем единственный результат
            System.out.println(petya.getUsername());
        }catch (NullPointerException n){
            //TODO is it norm?
            return null;
        }catch (Exception e) {
            System.err.println("Error in getUserByName(): " + e.getMessage());
        }
        System.out.println("Message from userRepository:"+petya.getUsername()+" "+petya.getPassword()+" "+petya.getAuthorities());
        return petya;
    }

    @Override
    public void updateUsers(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    @Override
    public User getUserById(Long id) {
        return (User)sessionFactory.getCurrentSession().load(User.class, id);
    }

    @Override
    public void deleteUser(User user) {
        sessionFactory.getCurrentSession().delete(user);
    }
}
