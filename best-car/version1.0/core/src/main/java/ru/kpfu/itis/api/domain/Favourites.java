package ru.kpfu.itis.api.domain;

import ru.kpfu.itis.api.domain.interfaces.WishList;

import javax.persistence.*;

/**
 * Created by kuzin on 09.03.2016.
 */

@Entity
public class Favourites extends WishList {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name = "user_id")
    private User user;

    @OneToOne
    @JoinColumn(name = "car_id")
    private Car car;

    //getters and setters

    public Favourites(){}

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
