package ru.kpfu.itis.api.repository.hibernateImpl;

import org.hibernate.SQLQuery;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import ru.kpfu.itis.api.domain.Favourites;
import ru.kpfu.itis.api.domain.User;
import ru.kpfu.itis.api.repository.iFavouritesRepository;

import java.util.List;

/**
 * Created by kuzin on 09.03.2016.
 */
@Repository
@Transactional
public class FavouritesRepository implements iFavouritesRepository {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void addFavourites(Favourites favourites){ sessionFactory.getCurrentSession().save(favourites);}

    @Override
    public List<Favourites> getAllFavourites(){
        return sessionFactory.getCurrentSession().createCriteria(Favourites.class).list();
    }

    @Override
    public void updateFavouritess(Favourites favourites) {
        sessionFactory.getCurrentSession().update(favourites);
    }

    @Override
    public Favourites getFavouritesById(Long id) {
        return (Favourites)sessionFactory.getCurrentSession().load(Favourites.class, id);
    }

    @Override
    public void deleteFavourites(Favourites favourites) {
        sessionFactory.getCurrentSession().delete(getFavouritesById(favourites.getId()));
    }

    @Override
    public List<Favourites> getAllFavouritesFor(User user){
        List<Favourites> favs=null;
        try {
            SQLQuery sqlQuery = sessionFactory.getCurrentSession()
                    .createSQLQuery("select * from favourites where user_id="+user.getId())
                    .addEntity(Favourites.class);
            favs = sqlQuery.list();
        } catch (Exception e) {
            System.err.println("Error in getAllFavouritesFor(): " + e.getMessage());
        }
        return favs;
    }
}
