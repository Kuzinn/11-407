package ru.kpfu.itis.api.Variables;

/**
 * Created by kuzin on 09.03.2016.
 */
public enum EngineType {
    GASOLINE,DIESEL,ELECTRIC,HYDROGEN;
}
