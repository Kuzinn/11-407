package DataAlgorithmsAndStructure.thirdlesson.homework;

/**
 * Created by Дмитрий on 03.03.2015.
 */
public class MyQueue implements Queue{
    private int[] array;
    private int lastindex=0;
    private int n=10;
    public MyQueue(){
        array=new int[n];
    }
    @Override
    public int pop() {
        int buf=array[0];
        int[] newarray=new int[--lastindex];
        System.arraycopy(array,1,newarray,0,newarray.length);
        array=newarray;
        return buf;
    }

    @Override
    public void push(int value) {
        if (array.length==lastindex){
            int[] newarray=new int[n*=2];
            System.arraycopy(array,0,newarray,0,array.length);
            array=newarray;
        }
        array[lastindex++]=value;

    }

    @Override
    public int size() {
        return lastindex;
    }

}
