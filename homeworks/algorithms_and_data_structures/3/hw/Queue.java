package DataAlgorithmsAndStructure.thirdlesson.homework;

/**
 * Created by Дмитрий on 03.03.2015.
 */
public interface Queue {
    public int pop();
    public void push(int x);
    public int size();
}
