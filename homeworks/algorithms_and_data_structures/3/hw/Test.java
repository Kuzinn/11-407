package DataAlgorithmsAndStructure.thirdlesson.homework;

/**
 * Created by Дмитрий on 03.03.2015.
 */
public class Test {
    public static void main(String[] args)  {
        MyQueue queue=new MyQueue();
        int n=10;
        while(n<1000000000) {
            long java1 = System.currentTimeMillis();
            for (int i = 0; i < n; i++) {
                queue.push(i);
            }
            long java2 = System.currentTimeMillis();
            long java3=System.currentTimeMillis();
            for (int i = 0; i < n; i++) {
                queue.pop();
            }
            long java4=System.currentTimeMillis();
            System.out.println(n+" " +"Вывод-"+(java4-java3));
            n*=10;
        }
    }
}
