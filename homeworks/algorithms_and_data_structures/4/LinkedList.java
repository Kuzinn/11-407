package DataAlgorithmsAndStructure.five_lesson210315;


/**
 * Created by Дмитрий on 21.03.2015.
 */
public class LinkedList<T> {//односвязный список
    private Link first;
    private Link last;
    public void addFirst(T data){
        Link link=new Link();
        link.data=data;
        if(first==null){
            first=link;
            last=link;
        }else{
            link.next=first;
            first=link;
        }

    }
    public void addLast(T data){
        Link link=new Link();
        link.data=data;
        if(last==null){
            first=link;
            last=link;
        }else{
            last.next=link;
            last=link;
        }
    }
    public void printList(){
        Link a=first;
        while(a!=null){
            System.out.println(a.data+" ");
            a=a.next;
        }
    }
}