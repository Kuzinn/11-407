package DataAlgorithmsAndStructure.firslesson.hw;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by Дмитрий on 18.02.2015.
 */
public class Far {//https://docs.google.com/spreadsheets/d/173TF5N95LOHRkSSohL6E52qTkKUG9mImqwxKFw1UVF4/edit?usp=sharing
                  //  ^
                  // /|\
                  //  |   график сравнения алгоритмов
    public static void main(String[] args) {
        ArrayList<Farey> list = new ArrayList<Farey>();
        Scanner sc = new Scanner(System.in);
        System.out.println("n=?");
        int n = sc.nextInt();
        long startTime=System.currentTimeMillis();
        list.add(new Farey(0, 1, 1));
        list.add(new Farey(1, 1, 0));
        for (int i = 2; i <= n; i++) {
            for (int j = 0; j < list.size()-1; j++) {
                if (i > 2) {
                    int next = list.get(j).getNext();
                    int p = list.get(j).getP() + list.get(next).getP();
                    int q = list.get(j).getQ() + list.get(next).getQ();
                    if (q <= n) {
                        list.add(new Farey(p, q, next));
                        list.get(j).changeNext(list.size()-1);
                        j++;
                    }
                } else {

                    int next = list.get(j).getNext();
                    int p = list.get(j).getP() + list.get(next).getP();
                    int q = list.get(j).getQ() + list.get(next).getQ();
                    if (q <= n) {
                        list.add(new Farey(p, q, next));
                        list.get(j).changeNext(list.size()-1);
                        j++;
                    }
                }
            }
        }
        long endTime=System.currentTimeMillis();
//        for (Farey c : list) {
//            System.out.print(c.getP() + "/" + c.getQ() + "|"+c.getNext()+"\r\n");
//        }//печать
        System.out.println("Farey algorithm time "+"for "+n+" steps ->  "+ (endTime-startTime));


    }
}

class Farey {
    private int p;
    private int q;
    private int next;

    Farey(int p, int q, int next) {
        this.p = p;
        this.q = q;
        this.next = next;
    }

    int getP() {
        return p;
    }

    int getQ() {
        return q;
    }

    int getNext() {
        return next;
    }

    void changeNext(int a) {
        this.next = a;
    }

}