
public class BinarySearch {
    int[] arr;
    String[] suffarray;

    public BinarySearch(int[] arr1, String[] arr2) {
        arr = arr1;
        suffarray = arr2;
    }

    public String search(String a) {//найти суффиксы которые начинаются на a
        String out="";
        int first = 0;
        int last = arr.length - 1;
        int mid = (last - first) / 2;
        boolean isEnd=false;
        while(!isEnd) {
            if(suffarray[mid].length()<a.length()){
                break;
            }
            if (a.compareTo(suffarray[0]) < 0 || a.compareTo(suffarray[suffarray.length - 1]) > 0) {
                if(suffarray[0].substring(0, a.length()).equals(a)){
                    Integer ac=arr[0];
                    out+=ac.toString();
                    int i=0;
                    while(true){
                        i++;
                        if(suffarray[i].substring(0, a.length()).equals(a)){
                            Integer acb=arr[i];
                            out+=" ,";
                            out+=acb.toString();
                        }else{
                            break;
                        }
                        isEnd=true;
                    }
                }else{
                    System.out.println("exception");
                    break;
                }
            } else if (a.compareTo(suffarray[mid]) == 0 && suffarray[mid].substring(0, a.length()).equals(a)) {
                Integer ac=arr[mid];
                out+=ac.toString();
                int i=0;
                while(true){
                    i++;
                    if(suffarray[mid-i].substring(0, a.length()).equals(a)){
                        Integer acb=arr[mid-i];
                        out+=" ,";
                        out+=acb.toString();
                    }else{
                        break;
                    }
                }
                break;
            } else if (a.compareTo(suffarray[mid]) > 0) {
                first = mid;
                mid = (last + first) / 2;
            } else if (a.compareTo(suffarray[mid]) < 0 && suffarray[mid].substring(0, a.length()).equals(a)) {
                Integer ac=arr[mid];
                out+=ac.toString();
                int i=0;
                while(true){
                    i++;
                    if(suffarray[mid-i].length()<a.length()){
                        break;
                    }
                    if(suffarray[mid-i].substring(0, a.length()).equals(a)){
                        Integer acb=arr[mid-i];
                        out+=" ,";
                        out+=acb.toString();
                    }else{
                        break;
                    }
                }
                break;
            } else if (a.compareTo(suffarray[mid]) < 0) {
                last = mid;
                mid = (last + first) / 2;
            }
        }
        return out;
    }
}
