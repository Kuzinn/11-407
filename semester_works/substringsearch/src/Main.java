import java.io.FileNotFoundException;


public class Main {
    public static void main(String[] args) {
        System.out.print("File name->");//принимаем имя файла
        String source="";
        try {
             source=FileWriter.readTextFrom(FileWriter.listen());
        } catch (FileNotFoundException e) {
            System.out.println("File do not exist!");
        }
        SuffArrayBuilder builder=new SuffArrayBuilder();
        builder.buildArray(source);//построилли и отсортировали суффиксный массив
        BinarySearch binarySearch=new BinarySearch(builder.arr,builder.suffarray);

        while(true){
            System.out.print("Search->");
            String searchString=FileWriter.listen();//принимаем строку которую нужно искать
            String a=binarySearch.search(searchString);
                System.out.println("Индексы вхождения подстроки в текст:");
                System.out.print("{ ");
                System.out.print(a);
                System.out.println("}");

        }

    }
}
