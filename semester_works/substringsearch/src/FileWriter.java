import java.io.*;
import java.util.Scanner;


public class FileWriter{
    public static String readTextFrom(String nameOfFile) throws FileNotFoundException {
        File file=new File(nameOfFile);
        if (!file.exists()) throw new FileNotFoundException();
        Scanner sc=new Scanner(new FileInputStream(file));
        String out="";
        while(sc.hasNext()){
            out+=sc.next();

        }
        return out;
    }
    public static String listen(){
        Scanner scanner=new Scanner(System.in);
        String out=scanner.next();
        return out;
    }
}
