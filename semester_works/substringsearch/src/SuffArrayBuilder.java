
public class SuffArrayBuilder {
    String[] suffarray;
    int[] arr;
    public void buildArray(String src){
        suffarray=new String[src.length()];
        arr=new int[src.length()];
        int i=0;
        String source=src;
        while(source.length()>0){
            suffarray[i]=source;
            arr[i]=i;
            i++;
            source=source.substring(1);
        }
        for (int k = 0; k < suffarray.length-1; k++) {
            for (int j = 0; j < suffarray.length-1; j++) {
                if(suffarray[j].compareToIgnoreCase(suffarray[j+1])>0){
                    String buf=suffarray[j];
                    suffarray[j]=suffarray[j+1];
                    suffarray[j+1]=buf;
                    int buff=arr[j];
                    arr[j]=arr[j+1];
                    arr[j+1]=buff;
                }
            }
        }
   }
}
