package SemiPrograms.lists;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;

/**
 * Created by Дмитрий on 21.03.2015.
 */
public class Variant3 {//Вариант 3: Программа последовательно читает числа и вставляет их в упорядоченный двусвязный список
    // (поддерживая его упорядоченность). После завершения чтения, программа перемешивает содержимое списка,
// используя рандомизированную сортировку слиянием (вместо выбора наибольшего/наименьшего элемента при сравнении пары чисел, выбирать случайный).
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя файла:");
        File file = new File(sc.next() + ".txt");
        LinkedList<Integer> list = new LinkedList<Integer>();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            Scanner scanner = new Scanner(fileInputStream);
            while (scanner.hasNext()) {
                list.add(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        int[] ab=new int[list.size()];
        for (int i = 0; i < list.size(); i++) {
            ab[i]=list.get(i);
        }
        ab=sort(ab);
        for (int i = 0; i < ab.length; i++) {
            System.out.println(ab[i]);
        }
    }
    public static int[] sort(int[] arr){//using recursion
        if(arr.length < 2) return arr;
        int m = arr.length / 2;
        int[] arr1 = Arrays.copyOfRange(arr, 0, m);
        int[] arr2 = Arrays.copyOfRange(arr, m, arr.length);
        return merge(sort(arr1), sort(arr2));
    }
    //merging of two arrays in one sorted array
    public static int[] merge(int[] arr1, int arr2[]){
        Random r=new Random();
        int n = arr1.length + arr2.length;
        int[] arr = new int[n];
        int i1=0;
        int i2=0;
        for(int i=0;i<n;i++){
            if(i1 == arr1.length){
                arr[i] = arr2[i2++];
            }else if(i2 == arr2.length){
                arr[i] = arr1[i1++];
            }else{
                if(r.nextInt()%2==0){//here should be compare
                    arr[i] = arr1[i1++];
                }else{
                    arr[i] = arr2[i2++];
                }
            }
        }
        return arr;
    }
}
