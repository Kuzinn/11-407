﻿package SemiPrograms.lists;
//я как то пропустил тот момент когда вы писали про варианты семестровок в чате.Поэтому сделал и этот вариант.




import DataAlgorithmsAndStructure.thirdlesson.MyList;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Scanner;

/**
 * Created by Дмитрий on 19.03.2015.
 */
public class Main {
//    Вариант 1: Программа последовательно читает числа и вставляет их в упорядоченный односвязный список
// (поддерживая его упорядоченность). После завершения чтения, программа выводит содержимое списка на экран путем его обхода.
// Затем программа переворачивает список (направления связей изменяются на противоположные, хвост становится головой) и выводит его снова.

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя файла:");
        String s = sc.next();
        File file = new File(s + ".txt");
        LinkedList<Integer> list = new LinkedList<Integer>();
        try {
            FileInputStream fileInputStream = new FileInputStream(file);
            Scanner scanner = new Scanner(fileInputStream);
            while (scanner.hasNext()) {
                list.addLast(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        list.printList();
        list.rotateList();
        System.out.println("rotated list:");
        list.printList();
    }
}
