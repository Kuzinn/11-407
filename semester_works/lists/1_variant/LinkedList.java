package SemiPrograms.lists;


/**
 * Created by Дмитрий on 20.03.2015.
**/
public class LinkedList<T> {//односвязный список
    private Link first;
    private Link last;
    public void rotateList(){
        Link linkOnFirst=first;
        last.next=first;
        Link buf=last;
        last=findLast();
        first=buf;
        last.next=null;
        Link l=first;
        while(last.data!=linkOnFirst.data){
            Link buff=findLast();
            last.next=linkOnFirst;
            l.next=last;
            l=last;
            last=buff;
            last.next=null;
        }
    }
    private Link findLast() {
        Link link = first;
        while (link.next != last) {
            link = link.next;
        }
        return link;
    }
    public void addLast(T data){
        Link link=new Link();
        link.data=data;
        if(last==null){
            first=link;
            last=link;
        }else{
            last.next=link;
            last=link;
        }
    }
    public void printList(){
        Link a=first;
        while(a!=null){
            System.out.println(a.data+" ");
            a=a.next;
        }
    }
}

class Link<T>{
    T data;
    Link next;
}

