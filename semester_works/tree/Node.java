package SemiPrograms.tree;

/**
 * Created by Дмитрий on 04.04.2015.
 */
public class Node {
    private Node left;
    private Node right;
    private int value;
    private int key;
    public Node getLeft(){
        return left;
    }
    public Node getRight(){
        return right;
    }
    public void setLeft(Node link){
        left=link;
    }
    public void setRight(Node link){
        right=link;
    }
    public void setValue(int number){
        value=number;
    }
    public void setKey(int a){
        key=a;
    }
    public int getValue(){
        return value;
    }
    public int getKey(){
        return key;
    }
}
