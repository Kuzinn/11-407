package SemiPrograms.tree;



import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Дмитрий on 04.04.2015.
 */
public class Tree {
    private Node root;
    public Node getRoot(){
        return root;
    }
    public void add(int key,int value){

        if(root==null){
            root=new Node();
            root.setValue(value);
            root.setKey(key);
        }else{
            Node a=root;
            Node b=new Node();
            b.setValue(value);
            b.setKey(key);
            boolean k=false;
            while(!k) {
                if (key >= a.getKey()) {
                    if (a.getRight() == null) {
                        a.setRight(b);
                        k = true;
                    } else {
                        a = a.getRight();

                    }
                }else{
                    if(a.getLeft()==null){
                        a.setLeft(b);
                        k=true;
                    }else{
                        a=a.getLeft();

                    }
                }
            }
        }
    }
    public void remove(int key,int value){//доделать
        Node a=root;
        while (key != a.getKey()) {
            if (key > a.getKey()) {
                a = a.getRight();
            } else {
                a= a.getLeft();
            }
        }
        if((a.getRight()==null)&&(a.getLeft()==null)){
            a=null;
        }else if((a.getLeft()!=null)&&(a.getRight()!=null)){

        }

    }
    public int get(int key) throws NullPointerException{
        Node a = root;
        while (key != a.getKey()) {
            if (key > a.getKey()) {
                a = a.getRight();
            } else {
                a= a.getLeft();
            }
        }
        //если такого элемента нет все равно вывожу что то
        if(a==null) {
            throw new NullPointerException();
        }else{
            return a.getValue();
        }
    }
    public void printTree(){
        Queue<Node> q=new LinkedList<Node>();
        q.add(root);
        Node n=null;
        int count=0;
        int a=12;
        System.out.print("               ");
        while(!q.isEmpty()) {
            Node x = q.remove();
            if(x==root){
                q.add(n);
            }
            if(x==null){
                System.out.println("");
                for (int i = 0; i < a; i++) {
                    System.out.print(" ");
                }
                a-=4;
                q.add(n);
                count++;
                if(count>10){
                    break;
                }
                continue;

            }
            if(x!=null) {
                System.out.print(x.getKey() + "   ");
            }
            if (x.getLeft() != null)   // x.left - левое поддерево
                q.add(x.getLeft());
            if (x.getRight() != null) {  // x.right - правое поддерево
                q.add(x.getRight());
            }
            if(x==root){
                q.add(n);
            }

        }
    }
    public void round(Node n){
        System.out.println(n.getKey());
        if(n.getRight()!=null){
            round(n.getRight());

        }
        if(n.getLeft()!=null){
            round(n.getLeft());
        }
    }

}
