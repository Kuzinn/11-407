package SemiPrograms.tree;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Created by Дмитрий on 04.04.2015.
 */
public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scanner=new Scanner(System.in);
        String filename= scanner.next();
        File file=new File(filename);
        FileInputStream stream=new FileInputStream(file);
        Scanner sc=new Scanner(stream);
        Tree t=new Tree();
        while(sc.hasNext()){
            int a=sc.nextInt();
            t.add(a,a);
        }
       t.printTree();

    }
}
