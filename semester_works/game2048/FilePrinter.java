package SemiPrograms.game2048;

import java.io.*;
import java.util.Scanner;

/**
 * Created by Дмитрий on 13.03.2015.
 */
public class FilePrinter {
    private String nameOfFile="";
    public void giveNameToFile(String yourName){
        nameOfFile=yourName;
    }
    public void saveLastMove(int[][] a){
        File file1 = new File("undo.txt");
        try {
            file1.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            PrintWriter pw=new PrintWriter(file1);
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    pw.print(a[i][j]);
                    pw.print(" ");
                }
            }
            pw.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
    public int[][] undo() {
        int[][] field = new int[4][4];
        try {
            FileInputStream fileInputStream = new FileInputStream("undo.txt");
            Scanner sc = new Scanner(fileInputStream);
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    field[i][j] = sc.nextInt();
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return field;
    }
    public  void saveGame(int[][] a,int score)  {
        File file = new File("Last_records_of_"+nameOfFile+".txt");

        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            PrintWriter printWriter=new PrintWriter(file);

            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    printWriter.print(" ");
                    printWriter.print("[");
                    printWriter.print(a[i][j]);
                    printWriter.print("]");
                }
                printWriter.println("");
            }
            printWriter.println("Score:");
            printWriter.print(score);
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }
    public void printInConsole(int[][] a){
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                if (a[i][j] == 2) {
                    System.out.print((char) 27 + "[31m" + "[" + "  " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 0) {
                    System.out.print((char) 27 + "[37m" + "[" + "  " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 4) {
                    System.out.print((char) 27 + "[32m" + "[" + "  " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 8) {
                    System.out.print((char) 27 + "[33m" + "[" + "  " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 16) {
                    System.out.print((char) 27 + "[34m" + "[" + " " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 32) {
                    System.out.print((char) 27 + "[35m" + "[" + " " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 64) {
                    System.out.print((char) 27 + "[36m" + "[" + " " + a[i][j] + " " + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 128) {
                    System.out.print((char) 27 + "[37m" + "[" + " " + a[i][j] + "" + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 256) {
                    System.out.print((char) 27 + "[38m" + "[" + " " + a[i][j] + "" + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 512) {
                    System.out.print((char) 27 + "[39m" + "[" + " " + a[i][j] + "" + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 1024) {
                    System.out.print((char) 27 + "[40m" + "[" + "" + a[i][j] + "" + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] == 2048) {
                    System.out.print((char) 27 + "[41m" + "[" + "" + a[i][j] + "" + "]" + (char) 27 + "[0m" + " ");
                } else if (a[i][j] < 1000) {
                    System.out.print("[" + "" + a[i][j] + "]" + "");
                } else {
                    System.out.print("[" + a[i][j] + "]" + " ");
                }
            }
            System.out.println("");
        }
    }
    public boolean equals(int[][] a,int[][] b){
        int countOfEqualsMembers=0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(a[i][j]==b[i][j]) countOfEqualsMembers++;
            }
        }
        if(countOfEqualsMembers==16){
            return true;
        }else{
            return false;
        }
    }
}
