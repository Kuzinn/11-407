﻿package SemiPrograms.game2048;

/**
 * Created by Дмитрий on 28.02.2015.
 */

import java.util.Random;
import java.util.Scanner;


/**
 * Created by Dmitriy on 28.02.2015.
 */
public class Main {
    public static boolean wasAddition;
    public static int countOfNullMembers = 16;
    public static int score=0;
    public static void main(String[] args)  {
        FilePrinter filePrinter=new FilePrinter();
        wasAddition=false;
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите ваше имя.");
        filePrinter.giveNameToFile(sc.next());
        int[][] a = new int[4][4];
        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < a.length; j++) {
                a[i][j] = 0;
            }
        }
        //first adding of two numbers in zero map
        try {
            addRandomNumbers(a);
        }catch(CountOfNullMembersLessThanZero e){
            e.getMessage();
        }
        filePrinter.printInConsole(a);
        //"game" stars here
        do {
            wasAddition=false;
            System.out.println("add direction");
            String commad = sc.next();
            commad.toLowerCase();
            char dir = commad.charAt(0);
            switch (dir) {
                case 'd':
                    filePrinter.saveLastMove(a);
                    try {
                        moveRirht(a);
                    }catch(IndexOutOfBoundsException e1){
                        System.out.println("Индекс вышел за пределы массива");
                    }
                    filePrinter.saveGame(a,score);

                    break;
                case 'a':
                    filePrinter.saveLastMove(a);
                    try{
                    moveLeft(a);
                    }catch(IndexOutOfBoundsException e2){
                        System.out.println("Индекс вышел за пределы массива");
                    }
                    filePrinter.saveGame(a,score);
                    break;
                case 'w':
                    filePrinter.saveLastMove(a);
                    try {
                        moveUp(a);
                    }catch(IndexOutOfBoundsException e3){
                        System.out.println("Индекс вышел за пределы массива");
                    }
                    filePrinter.saveGame(a,score);
                    break;
                case 's':
                    filePrinter.saveLastMove(a);
                    try {
                        moveDown(a);
                    }catch(IndexOutOfBoundsException e4){
                        System.out.println("Индекс вышел за пределы массива");
                    }
                    filePrinter.saveGame(a,score);
                    break;
                case 'p':
                    filePrinter.saveGame(a,score);
                    break;
                case 'u'://undo
                    a=filePrinter.undo();
                    filePrinter.printInConsole(a);
                    break;
                default:
                    System.out.println("Wrong button!");
                    break;
            }
            System.out.println("");
            // print(a);


            if(filePrinter.equals(filePrinter.undo(),a)==false) {
                try {
                    addRandomNumbers(a);
                } catch (CountOfNullMembersLessThanZero e1) {
                    e1.getMessage();
                }
            }
            System.out.println("");
            System.out.println("score:"+score);
            filePrinter.printInConsole(a);
            System.out.println("");

        }while (isEnd(a) == false);
        if(isHeWin(a)){
        System.out.println("Congratulations!!!");
        }else{
            System.out.println("Try again!");
        }

    }

    public static int[][] addRandomNumbers(int[][] a) throws CountOfNullMembersLessThanZero {
        Random r = new Random();

        int b = r.nextInt(4);
        if (b % 2 == 0) {
            b = 4;
        } else {
            b = 2;
        }
        int x, y;
        if (countOfNullMembers == 1) {
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (a[i][j] == 0) a[i][j] = b;

                }
            }
            countOfNullMembers--;
        } else if (countOfNullMembers == 16) {
            b = r.nextInt(4);
            if (b % 2 == 0) {
                b = 4;
            } else {
                b = 2;
            }
            x = r.nextInt(a.length - 1);
            y = r.nextInt(a.length - 1);
            int x1 = r.nextInt(a.length - 1);
            int y1 = r.nextInt(a.length - 1);
            while ((x == x1) && (y == y1)) {
                x1 = r.nextInt(a.length - 1);
                y1 = r.nextInt(a.length - 1);
            }
            a[x][y] = 2;
            a[x1][y1] = b;
            countOfNullMembers-=2;
        } else if ((countOfNullMembers < 16) && (countOfNullMembers > 1)) {
            int count = 1;
            int arr[][] = new int[4][4];
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (a[i][j] == 0) {
                        arr[i][j] = count;
                        count++;
                    }
                }
            }
            int randomPossition = 1 + (int) (Math.random() * ((countOfNullMembers - 1) + 1));
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 4; j++) {
                    if (arr[i][j] == randomPossition) {
                        int b1 = r.nextInt(5);
                        if (b1 % 2 == 0) {
                            a[i][j] = 4;
                        } else {
                            a[i][j] = 2;
                        }
                    }
                }
            }
            countOfNullMembers--;
            arr = null;
            count = 0;
        } else if(countOfNullMembers<0){
            throw new CountOfNullMembersLessThanZero();
        }


        return a;
    }

    public static int[][] moveRirht(int[][] a) throws IndexOutOfBoundsException{
        int prev=0;
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >= 0; j--) {
                if (a[i][j] == 0) {
                    prev++;
                }else{
                    if(prev>0){
                        a[i][j+prev]=a[i][j];
                        a[i][j]=0;

                    }
                }
            }
            prev=0;
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j > 0; j--) {
                if (a[i][j] == a[i][j - 1]) {
                    if(a[i][j]!=0) {
                        a[i][j] *= 2;
                        score += a[i][j];
                        wasAddition = true;
                        a[i][j - 1] = 0;
                        countOfNullMembers++;
                    }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >= 0; j--) {
                if (a[i][j] == 0) {
                    prev++;
                }else{
                    if(prev>0){
                        a[i][j+prev]=a[i][j];
                        a[i][j]=0;

                    }
                }
            }
            prev=0;
        }
        return a;
    }

    public static int[][] moveLeft(int[][] a) throws IndexOutOfBoundsException{
        int prev=0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j<4; j++) {
                if(a[i][j]==0){
                    prev++;
                }else {
                    if(prev>0) {
                        a[i][j - prev] = a[i][j];
                        a[i][j] = 0;

                    }
                }
            }
            prev=0;
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j <3; j++) {
                if (a[i][j] == a[i][j + 1]) {
                    if(a[i][j]!=0) {
                        a[i][j] += a[i][j+1];
                        score += a[i][j];
                        wasAddition = true;
                        a[i][j+1] = 0;
                        countOfNullMembers++;
                    }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j<4; j++) {
                if(a[i][j]==0){
                    prev++;
                }else{
                    if(prev>0) {
                        a[i][j - prev] = a[i][j];
                        a[i][j] = 0;

                    }
                }
            }
            prev=0;
        }

        return a;
    }

    public static int[][] moveUp(int[][] a) throws IndexOutOfBoundsException {
        int prev=0;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (a[j][i] == 0) {
                    prev++;
                }else{
                    if(prev>0) {
                        a[j - prev][i] = a[j][i];
                        a[j][i] = 0;

                    }
                }
            }
            prev=0;
        }
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 4; j++) {
                if (a[i][j] == a[i + 1][j]) {
                    if(a[i][j]!=0) {
                        a[i][j] += a[i + 1][j];
                        score += a[i][j];
                        wasAddition = true;
                        a[i + 1][j] = 0;
                        countOfNullMembers++;
                    }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if (a[j][i] == 0) {
                    prev++;
                }else{
                    if(prev>0) {
                        a[j - prev][i] = a[j][i];
                        a[j][i] = 0;

                    }
                }
            }
            prev=0;
        }
        return a;
    }

    public static int[][] moveDown(int[][] a) throws IndexOutOfBoundsException{
        int prev=0;
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >=0; j--) {
                if(a[j][i]==0){
                    prev++;
                }else{
                    if(prev>0){
                        a[j+prev][i]=a[j][i];
                        a[j][i]=0;
                    }
                }
            }
            prev=0;
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >0; j--) {
                if (a[j][i] == a[j - 1][i]) {
                    if(a[j][i]!=0) {
                        a[j][i] += a[j-1][i];
                        score += a[j][i];
                        wasAddition = true;
                        a[j-1][i] = 0;
                        countOfNullMembers++;
                    }
                }
            }
        }
        for (int i = 0; i < 4; i++) {
            for (int j = 3; j >=0; j--) {
                if(a[j][i]==0){
                    prev++;
                }else{
                    if(prev>0){
                        a[j+prev][i]=a[j][i];
                        a[j][i]=0;
                    }
                }
            }
            prev=0;
        }
        return a;
    }

    public static boolean isHeWin(int[][] a){
        boolean isHeWin=false;
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                if(a[i][j]==2048) isHeWin=true;
            }
        }
        return isHeWin;
    }

    public static boolean isEnd(int[][] a) {
        boolean endOfGame = false;
        if(countOfNullMembers==0) {
            endOfGame=true;
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    if (a[i][j] == a[i][j + 1]) endOfGame = false;
                }
            }
            for (int i = 0; i < 4; i++) {
                for (int j = 0; j < 3; j++) {
                    if (a[j][i] == a[j + 1][i]) endOfGame = false;
                }
            }

        }



        return endOfGame;
    }


}