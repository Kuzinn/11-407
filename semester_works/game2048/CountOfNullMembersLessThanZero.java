package SemiPrograms.game2048;

/**
 * Created by Дмитрий on 18.03.2015.
 */
public class CountOfNullMembersLessThanZero extends Exception {
    public String getMessage() {
        return "Кол-во нулевых элементов меньше нуля!";
    }
}
