package view;

import model.Field;

/**
 * Created by dk on 05.05.15.
 */
public class Viewer {//refresh console
    public Field opponentField=new Field();
    public Field myField=new Field();
    public Viewer(){
        opponentField.isOpponent(true);
        myField.isOpponent(false);
    }

    public void refreshTerminal(){
        TerminalUI ui=new TerminalUI();
        ui.printUI(opponentField.field, myField.field);
    }


}
