package view;


import model.utills.Listener;

/**
 * Created by dk on 05.05.15.
 */
public class TerminalUI {

    public void printUI(String[][] opponentField, String[][] field) {
        String[][] f = field;
        System.out.println("________________________________________________________________________________");
        System.out.println("");
        System.out.println("                                 BATTLESHIP");
        System.out.println("________________________________________________________________________________");
        System.out.println("");
        System.out.println("");
        System.out.println("Your field:                              Opponent field:");
        System.out.println("~|   1  2  3  4  5  6  7  8  9  10       |     ~|   1  2  3  4  5  6  7  8  9  10");
        for (int i = 0; i < 10; i++) {
            if (i != 9) {
                System.out.print(i + 1 + "   ");
            } else {
                System.out.print(i + 1 + "  ");
            }
            //printing client's field
            for (int j = 0; j < 10; j++) {
                if (f[i][j].equals("*")) {
                    System.out.print("[" + f[i][j] + "]");
                } else if(f[i][j].equals("x")){//if deck killed it would be red
                    System.out.print((char) 27 + "[31m" + " " + f[i][j] + " " + (char) 27 + "[0m");
                }else if(f[i][j].equals(".")){//if missed it would be yellow
                    System.out.print((char) 27 + "[33m" + " " + f[i][j] + " " + (char) 27 + "[0m");
                }else {//sea would be blue
                    System.out.print((char) 27 + "[34m" + " " + f[i][j] + " " + (char) 27 + "[0m");
                }
            }
            System.out.print("             ");
            if (i != 9) {
                System.out.print(i + 1 + "   ");
            } else {
                System.out.print(i + 1 + "  ");
            }
            //printing opponent's field
            for (int j = 0; j < 10; j++) {
                if (opponentField[i][j] == "x") {
                    System.out.print((char) 27 + "[31m" + " " + opponentField[i][j] + " " + (char) 27 + "[0m");
                } else if (opponentField[i][j] == ".") {
                    System.out.print((char) 27 + "[33m" + " " + opponentField[i][j] + " " + (char) 27 + "[0m");
                } else {
                    System.out.print(" " + opponentField[i][j] + " ");
                }
            }System.out.println("");
        }


    }
    public void printStartPage(){
        System.out.println("\n\n\n\n\n\n\n\n\n");
        System.out.println("                                       ...");
        System.out.println("                                BATTLESHIP OnLine");
        System.out.println("                             SemiWork by Dmitiy Kuzin");
        System.out.println("                                       ...");
        System.out.println("");
        try {
            Thread.sleep(1800);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
    public String asker(String string){
        Listener listener=new Listener();
        String out=null;
        switch (string){
            case "oppPrev":
                System.out.println("\n\n\n\n\n\n\n\n\n");
                System.out.println("________________________________________________________________________________");
                System.out.println("");
                System.out.println("                                 BATTLESHIP");
                System.out.println("________________________________________________________________________________");
                System.out.println("");
                System.out.println("                           -Are your opponent previous ?");
                System.out.print("                           -");
                out=listener.Listen();
                break;

            case "firstsecond":
                System.out.println("\n\n\n\n\n\n\n\n\n");
                System.out.println("________________________________________________________________________________");
                System.out.println("");
                System.out.println("                                 BATTLESHIP");
                System.out.println("________________________________________________________________________________");
                System.out.println("");
                System.out.println("                            -Договоритесь с другим пользователем кто будет первым");
                System.out.println("                            -Write 'first' or 'second'");
                System.out.print("                            ->");
                out= listener.Listen();
                break;
            case "congratulations":
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("Congratulations!You win!");
                System.out.println("\n\n\n\n");
                out= "";
                break;
            case "youlose":
                System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
                System.out.println("You lose!Try again!");
                System.out.println("\n\n\n\n");
                out= "";
                break;
            case "fireON":
                System.out.println();
                System.out.println("Fire on->");
                out=listener.Listen();
                break;
        }

        return out;
    }
}
