package model;

import model.ShootResult;

/**
 * Created by Дмитрий on 25.03.2015.
 */
public interface Ship{
    //Метод проверящию попал ли выстрел по этому кораблю
    public ShootResult getShootResult(int x, int y);
    public void setDeckLocation(int x,int y);
    public void setNextDeck(int x,int y);

}
