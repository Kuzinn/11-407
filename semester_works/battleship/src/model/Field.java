package model;

import model.utills.FileManager;

import java.io.*;

public class Field {

    public Shipp[] ships=new Shipp[10];//simple array of 'model.Shipp' classes
    public String[][] field;//used for print field in console
    private int lastFreeIndex=0;
    public void isOpponent(boolean a){
        field=new String[10][10];
        if(a){
            for (int i = 0; i < 10; i++) {
                for (int j = 0; j < 10; j++) {
                    field[i][j]="#";
                }
            }
        }else{
            try {
                setShips("field_Test.txt");
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
    public void setShips(String nameFile) throws FileNotFoundException {
        FileManager fm=new FileManager();
        field=fm.getField(nameFile);
        for (int i = 0; i < field.length; i++) {//setting ships
           M1: for (int j = 0; j < field.length; j++) {
                if(field[i][j].equals("*")){
                    int k;
                    //checking there is already a ship with such location
                    for (int l = 0; l < lastFreeIndex; l++) {
                        int[] x=ships[l].getX();
                        int[] y=ships[l].getY();
                        for (int m = 0; m < x.length; m++) {
                            if(i==x[m]&&j==y[m]) continue M1;
                        }
                    }
                    //create new ship
                    ships[lastFreeIndex]=new Shipp();
                    ships[lastFreeIndex].setDeckLocation(i,j);
                    //if there is other deck , call method setNextDeck

                        //if ship stretched horizontally
                        if ((j<9)&&(field[i][j + 1].equals("*"))) {
                            ships[lastFreeIndex].setNextDeck(i, j + 1);
                            k = j + 2;
                            while (true) {
                                if (field[i][k].equals("*")) {
                                    ships[lastFreeIndex].setNextDeck(i, k);
                                    k++;
                                } else {
                                    j = k;
                                    break;
                                }
                            }

                        } else if ((i<9)&&(field[i + 1][j].equals("*"))) {//if ship stretched upright
                            ships[lastFreeIndex].setNextDeck(i + 1, j);
                            k = i + 2;
                            while (true) {
                                if ((k<10)&&(field[k][j].equals("*"))) {
                                    ships[lastFreeIndex].setNextDeck(k, j);
                                    k++;
                                } else {
                                    break;
                                }
                            }
                        }

                        lastFreeIndex++;


                }
            }
        }
        for (int i = 0; i < lastFreeIndex; i++) {
            ships[i].deleteZeros();
        }
    }

}

