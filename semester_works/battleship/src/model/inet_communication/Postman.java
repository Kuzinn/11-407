package model.inet_communication;

public class Postman {//datagram
    private int port=0;
    private int opponentPort=0;
    private String address="";
    Thread t1=null;
    Thread t2=null;
    iSender sender=null;
    iReceiver receiver=null;
    public Postman(String firstOrSecond,String anotherUserIpAddress){//порт для приема будет отличаться на +1
        firstOrSecond = firstOrSecond.toLowerCase().trim().substring(0, 1);
        port=2000 - firstOrSecond.hashCode();
        if(firstOrSecond.equals("f")){
            opponentPort=2000-"s".hashCode();
        }else{
            opponentPort=2000-"f".hashCode();
        }

        address=anotherUserIpAddress;
        sender=new iSender(port,opponentPort,address);
        receiver=new iReceiver(port+1,opponentPort+1,address);
        t1=new Thread(sender);
        t2=new Thread(receiver);
        t1.setDaemon(true);
        t2.setDaemon(true);
        t1.start();
        t2.start();
            t1.suspend();
            t2.suspend();

    }
    public void send(String out){
        sender.setMessage(out);
        t1.resume();
            t1.suspend();

    }
    public String receive(){
        t2.resume();
            t2.suspend();
        String answer=receiver.getMessage();

        return answer;
    }
}
