package model.inet_communication;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * Created by dk on 05.05.15.
 */
public class iSender extends Thread {
    private int port=0;
    private int yourport=0;
    private String address="";
    private String message="";
    private DatagramSocket socket= null;
    public iSender(int yourPort,int anotherPort,String Address){
        port=anotherPort;
        yourport=yourPort;
        address=Address;

        try {
            socket = new DatagramSocket(yourport);
            socket.connect(new InetSocketAddress(address,port));
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }
    public void setMessage(String x){
        message=x;
    }
    @Override
    public void run() {
        try {

            DatagramPacket packet = new DatagramPacket(message.getBytes(), message.getBytes().length);
            socket.send(packet);

        } catch (SocketException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
