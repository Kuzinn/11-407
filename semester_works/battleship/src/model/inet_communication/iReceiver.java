package model.inet_communication;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetSocketAddress;
import java.net.SocketException;

/**
 * Created by dk on 05.05.15.
 */
public class iReceiver extends Thread {
    private int yourport=0;
    private int port=0;
    private String address="";
    private String message="";
    private DatagramSocket socket=null;
    public iReceiver(int yourPort,int anotherPort,String Address){
        yourport=yourPort;
        port=anotherPort;
        address=Address;
        try {
            socket = new DatagramSocket(yourport);
            socket.connect(new InetSocketAddress(address,port));
        } catch (SocketException e) {
            e.printStackTrace();
        }

    }
    public String getMessage(){
        while(message.equals("")){
            message="";
        }
        return message;
    }
    @Override
    public void run() {
        DatagramPacket packet = new DatagramPacket(new byte[5],5);
        try {
            socket.receive(packet);
            message=packet.getData().toString();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
