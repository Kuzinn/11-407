package model;

/**
 * Created by Дмитрий on 25.03.2015.
 */
public class Shipp implements Ship {
    private Deck lastDeck=new Deck();//by default all ships are singe-deck ships
    private int[] a,b;//location of ship
    private int lastindex=0;//count of decks
    private int count=0;
    public boolean killed(){
        boolean a=false;
        if(count>=lastindex) a=true;
        return a;
    }
    public int[] getX(){
        return a;
    }
    public int[] getY() { return b; }
    public void setDeckLocation(int x,int y){//set main deck location
        a=new int[4];
        b=new int[4];
        lastDeck.x=x;
        lastDeck.y=y;
        a[lastindex]=x;
        b[lastindex]=y;
        lastindex++;
    }
    public void setNextDeck(int x,int y){
        Deck newDeck=new Deck();
        newDeck.x=x;
        newDeck.y=y;
        lastDeck.next=newDeck;
        newDeck.prev=lastDeck;
        lastDeck=newDeck;
        a[lastindex]=x;
        b[lastindex]=y;
        lastindex++;
    }
    public void deleteZeros(){//deleting zeros from location arrays
        int[] newaray=new int[lastindex];
        System.arraycopy(a,0,newaray,0,lastindex);
        a=newaray;
        System.arraycopy(b,0,newaray,0,lastindex);
        b=newaray;
    }
    public ShootResult getShootResult(int x, int y){
        ShootResult sr= ShootResult.Missed;
        Deck deck=lastDeck;
        while(deck!=null){
            if((deck.x==x)&&(deck.y==y)){

                if(deck.killed){
                    sr= ShootResult.AlreadyDead;
                }else{
                    sr= ShootResult.Wounded;
                    deck.killed=true;
                    count++;
                }

                break;
            }
            deck= deck.prev;

        }
        if(count==lastindex) {
            sr= ShootResult.Killed;
            count++;
        }
        return sr;
    }

}
