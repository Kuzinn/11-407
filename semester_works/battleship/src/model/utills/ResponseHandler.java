package model.utills;

import model.Field;
import model.Shipp;
import model.inet_communication.Postman;

import java.util.Scanner;

/**
 * Created by dk on 05.05.15.
 */
public class ResponseHandler {
    public static String ipRequest(String x){
        FileManager fm=new FileManager();
        String address;
        if (x.toLowerCase().substring(0, 1).equals("y")) {
            address = fm.readIp();
            System.out.println("                           -Ok.Lets do some business!");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.print("\n\n\n\n\n\n\n");
        } else {
            System.out.print("                           -Add new ip->");
            Scanner sc=new Scanner(System.in);
            address = sc.next();//ip address
            fm.writeIp(address);
            System.out.println("");
            System.out.println("                           -Ok Cap,socket have already connected.");
            System.out.println("                           -I think we gona win today !");
        }
        return address;
    }
    public static String locationRequest(String out){
        Listener listener=new Listener();
        String[] input = out.split(",");
        Integer x;
        Integer y;
        while(true) {
            try {//to mention a shot at opponent field
                x = Integer.valueOf(input[0]);
                if (x > 10) throw new NumberFormatException();
                y = Integer.valueOf(input[1]);
                if (y>10) throw new NumberFormatException();
            } catch (NumberFormatException e) {
                System.out.println("Ввели неправильный формат координат! используйте -help для вызова легенды");
                out=listener.Listen();
            } catch (IndexOutOfBoundsException e) {
                System.out.println("Ввели неправильный формат координат! используйте -help для вызова легенды");
                out=listener.Listen();
            }
            break;
        }
        return out;
    }
    public static boolean QueueRequest(String x){//нет обработки ошибок
        boolean answer=false;
        if(x.substring(0,1).toLowerCase().equals("f")){
            answer=true;
        }
        return answer;
    }
    public static String isFirstRequest(String x){//нет обработки ошибок
        String answer="second";
        if(x.substring(0,1).toLowerCase().equals("f")){
            answer="first";
        }
        return answer;
    }
    public static boolean ShootRequest(String[][] field,String str){//строка вида 1,1|killed
        boolean answer=false;
        str=str.trim();
        if (str.equals("yw")){
            iWin();
        }
        String result=".";
        String[] str1=str.split("|");
        if((str1[1].equals("killed"))||(str1[1].equals("wounded"))||(str1[1].equals("alreadydead"))){
            answer=true;
            result="x";
        }

        String[] input1 = str1[0].split(",");
        Integer x = Integer.valueOf(input1[0]);
        Integer y = Integer.valueOf(input1[1]);
        x -= 1;
        y -= 1;
        field[x][y]=result;
        return answer;
    }
    public static boolean endRequest(Field myField){
        int count=0;
        for(Shipp p:myField.ships){
            if(p.killed()) count++;
        }
        if(count>=10) return true;
        return false;
    }
    public static void notifyOpponent(Postman postman){
        postman.send("yw");
    }
    public static void iWin(){
        System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");
        System.out.println("You win!");
        System.out.println("\n\n\n\n\n");
        try {
            Thread.sleep(1000000000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
