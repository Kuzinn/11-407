package model.utills;

import java.io.*;
import java.util.Scanner;


public class FileManager {
    File f;
    public FileManager(){
        f=new File("ip.txt");
        if(!f.exists()){
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String readIp(){
        String ip="";
        try {
            Scanner scanner=new Scanner(new FileInputStream(f));
            ip=scanner.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return ip;
    }
    public void writeIp(String ip){
        PrintWriter pw=null;
        try {
            pw=new PrintWriter(f);
            pw.print(ip);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }finally{
            pw.close();
        }
    }
    public String[][] getField(String path) throws FileNotFoundException {
        File file=new File(path);
        Scanner sc=new Scanner(new FileInputStream(file));//use stream FileInputStream for reading file with field
        String[] string=new String[10];//array of strings like '*#####**###'
        for (int i = 0; i < 10; i++) {
            if(sc.hasNextLine()) {
                string[i] = sc.nextLine();
            }
        }
        String[][] f=new String[10][10];//converting string[] into string[][]
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if(j==9){
                    f[i][j]=string[i];
                    break;
                }else {
                    f[i][j] = string[i].substring(0, 1);
                }
                string[i]=string[i].substring(1);
            }
        }
        return f;
    }
}
