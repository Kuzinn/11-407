package model;

/**
 * Created by Дмитрий on 25.03.2015.
 */


public enum ShootResult {
    //Мимо
    Missed,

    //Ранен
    Wounded,

    //Уже убит
    AlreadyDead,

    //Убит
    Killed
}