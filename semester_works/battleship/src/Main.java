import model.inet_communication.Postman;
import model.utills.ResponseHandler;
import view.TerminalUI;
import view.Viewer;


/**
 * Created by dk on 05.05.15.
 */
public class Main {
    public static void main(String[] args) {
        TerminalUI ui=new TerminalUI();
        Viewer viewer=new Viewer();
        ui.printStartPage();

        String address= ResponseHandler.ipRequest(ui.asker("oppPrev"));
        String isFirst=ResponseHandler.isFirstRequest(ui.asker("firstsecond"));
        Postman postman=new Postman(isFirst,address);
        boolean swap = ResponseHandler.QueueRequest(isFirst);
        viewer.refreshTerminal();
        while(!ResponseHandler.endRequest(viewer.myField)) {

            while (swap) {//shoot

                postman.send(ResponseHandler.locationRequest(ui.asker("fireON")));
                String answer = postman.receive();
                System.out.println("Received a message!");
                swap = ResponseHandler.ShootRequest(viewer.opponentField.field, answer);
                viewer.refreshTerminal();
            }
            while (!swap) {//listen
                String answer=postman.receive();
                swap = ResponseHandler.ShootRequest(viewer.myField.field, answer);
                viewer.refreshTerminal();
            }
        }
        ResponseHandler.notifyOpponent(postman);
    }
}
