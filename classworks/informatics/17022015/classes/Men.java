package SecondSemestrITIS.third_lesson.classes;

import SecondSemestrITIS.third_lesson.interfaces.Living;
import SecondSemestrITIS.third_lesson.interfaces.Movable;
import SecondSemestrITIS.third_lesson.interfaces.Thinking;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public class Men implements Movable,Living,Thinking {


    @Override
    public void move() {
        System.out.println("I can mooove!");
    }

    @Override
    public void think() {

    }

    @Override
    public void eat() {

    }

    @Override
    public void breath() {

    }

    @Override
    public void sleep() {

    }
}
