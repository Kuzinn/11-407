package SecondSemestrITIS.third_lesson.classes;

import SecondSemestrITIS.third_lesson.interfaces.Living;
import SecondSemestrITIS.third_lesson.interfaces.Movable;

import static SecondSemestrITIS.third_lesson.classes.Mood.Good;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public class Dog implements Movable,Living {

    @Override
    public void move() {
        System.out.println("Woof!!");
    }

    @Override
    public void eat() {
        System.out.println("I want eat!!!");
    }

    public void eat(Mood x){
        switch (x){
            case Good:
                System.out.println("Im fine,but u can give me this sausage.");
                break;
            case Hungry:
                System.out.println("Give me food.");
                break;
            case VeyHungry:
                System.out.println("Give me mooore food!!!");
                break;
            default:
                break;
        }
    }
    @Override
    public void breath() {

    }

    @Override
    public void sleep() {

    }

}
