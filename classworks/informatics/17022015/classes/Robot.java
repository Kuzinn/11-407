package SecondSemestrITIS.third_lesson.classes;

import SecondSemestrITIS.third_lesson.interfaces.Movable;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public class Robot implements Movable {
    @Override
    public void move() {
        System.out.println("Yes baby i am movable!");
    }
}
