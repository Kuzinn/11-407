package SecondSemestrITIS.third_lesson.interfaces;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public interface Living {
    public void eat();
    public void breath();
    public void sleep();
}
