package SecondSemestrITIS.third_lesson.interfaces;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public interface Movable {
    public void move();
}
