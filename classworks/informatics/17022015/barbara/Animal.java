package SecondSemestrITIS.third_lesson.barbara;

/**
 * Created by Дмитрий on 17.02.2015.
 */
abstract class Animal implements Living,Walk {
    public static void main(String[] args) {
        Animal a=new Cat();
        a.eat();
        Living b=new Cat();
        b.eat();
        Walk c=new Dog();
        c.walk();
    }
}
