package SecondSemestrITIS.third_lesson.barbara;

/**
 * Created by Дмитрий on 17.02.2015.
 */
public interface Living {
    public void eat();
    public void sleep();
    public void breath();
}
