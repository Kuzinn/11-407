package SecondSemestrITIS.fourlesson;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class ThreeArgumentException extends Exception {
    @Override
    public String getMessage() {
        return "You can't use argument equals three.";
    }
}
