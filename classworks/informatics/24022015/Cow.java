package SecondSemestrITIS.fourlesson;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class Cow extends Animal {
    @Override
    public void eat(Food x) throws FoodException {
        if(x.equals(Food.Grass)){
            System.out.println("Yees men,i can eat it.");
        }else{
            throw new FoodException();
        }
    }
}
