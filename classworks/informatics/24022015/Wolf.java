package SecondSemestrITIS.fourlesson;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class Wolf extends Animal {
    @Override
    public void eat(Food x) throws FoodException{
        if(x.equals(Food.Grass)){
            throw new FoodException();
        }else{
            System.out.println("I can eat it.");
        }
    }
}
