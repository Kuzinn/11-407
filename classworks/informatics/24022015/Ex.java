package SecondSemestrITIS.fourlesson;

import SecondSemestrITIS.third_lesson.barbara.Dog;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class Ex {
    public static void main(String[] args)  {
        try {
            int a = 5;
            int b = 0;
            int c = a / b;
        } catch (ArithmeticException a) {
            System.out.println("Обработка исключения деления на ноль");
        }
        try {
            Dog vasya = null;
            vasya.eat();
        } catch (NullPointerException b) {
            System.out.println("Обработка исключения ссылки на null");
        }
        try{
            int[] array = new int[2];
            for (int i = 0; i < 4; i++) {
                array[i] = i;
            }
        }catch (ArrayIndexOutOfBoundsException c) {
            System.out.println("Обработка исключения переполнения массива");
        }
        try{
            plus(3,5);
        }catch(ThreeArgumentException  d){
            System.out.println("we catch it!");
        }
        try {
            plus(5,3);
        } catch (ThreeArgumentException e) {
            e.getStackTrace();//показывает историю вызова мемтодов нужно для отладки нужная вешь
            System.out.println(e.getMessage());
        }
    }
    public void move(Dog x){
        if(x==null) throw new NullPointerException();
        x.eat();
    }
    public static int plus(int a,int b) throws ThreeArgumentException {
        if (a==3|b==3){
            throw new ThreeArgumentException(); }
        return a+b;
    }
}
