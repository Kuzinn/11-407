package SecondSemestrITIS.fourlesson;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class FoodException extends RuntimeException{
    @Override
    public String getMessage() {
        return "Wrong food!";
    }
}
