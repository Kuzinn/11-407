package SecondSemestrITIS.fourlesson;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class main {
    public static void main(String[] args) {
        Cow a=new Cow();
        Wolf b=new Wolf();
        Human c=new Human();
        try {
            a.eat(Food.Hamburger);
        }catch(FoodException h){
            System.out.println(h.getMessage());
        }
        try {
            b.eat(Food.Grass);

        }catch(FoodException h){
            System.out.println(h.getMessage());
        }
        try {
            c.eat(Food.Hamburger);
        }catch(FoodException h){
            System.out.println(h.getMessage());
        }

    }
}
