package SecondSemestrITIS.fourlesson;
import SecondSemestrITIS.third_lesson.barbara.Dog;

import java.io.*;

/**
 * Created by Дмитрий on 24.02.2015.
 */
public class M {
    public static void main(String[] args) throws IOException,NullPointerException {
        try {
            Dog x = null;
            x.eat();
        } catch (Exception e) {
            System.out.println("Ohh noooooo. Your program isn't work!");
        }
        test1();
        test2();
    }


    public static void test1()  {
        FileInputStream stream=null;
        try{
            stream.read();
        }catch(IOException d){
            System.out.println("We catch IOException");
        }catch(NullPointerException x ){
            System.out.println("We catch NullPointerException");
        }catch (Exception b){
            System.out.println("I don't know your exception");
        }
        finally{
            try {
                stream.close();
            } catch (Exception f) {
                System.out.println("We catch IOException");
            }
        }
    }
    public static void test2() throws IOException{
        FileInputStream stream=new FileInputStream("345.txt");
        stream.read();
    }


}
