package SecondSemestrITIS.first_lesson.Big_Calculator;


/**
 * Created by Дмитрий on 07.02.2015.
 */
public class TrigCalc implements SinCalculator {
    public double sin(double x){return Math.sin(x);}
    public double cos(double x){return Math.cos(x);}
    public double tg (double x){return Math.sin(x)/Math.cos(x);}
    public double ctg(double x){return Math.cos(x)/Math.sin(x);}
}
