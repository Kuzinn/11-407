package SecondSemestrITIS.first_lesson.Big_Calculator;

/**
 * Created by Дмитрий on 07.02.2015.
 */
public interface SimpleCalculator {
    public double sum(double x, double y);
    public double minus(double x,double y);
    public double div(double x,double y);
    public double mult(double x,double y);
}
