package SecondSemestrITIS.first_lesson.Big_Calculator;

class BigCalc implements SinCalculator, SimpleCalculator {
    SimpleCalc sCalc=new SimpleCalc();
    TrigCalc tCalc=new TrigCalc();
    @Override
    public double sum(double x, double y) {
        return sCalc.sum(x,y);
    }
//так же всё остальное
    @Override
    public double minus(double x, double y) {
        return sCalc.minus(x,y);
    }

    @Override
    public double div(double x, double y) {
        return sCalc.div(x,y);
    }

    @Override
    public double mult(double x, double y) {
        return sCalc.mult(x,y);
    }

    @Override
    public double sin(double x) {return tCalc.sin(x);}

    @Override
    public double cos(double x) {
        return tCalc.cos(x);
    }

    @Override
    public double tg(double x) {
        return tCalc.tg(x);
    }

    @Override
    public double ctg(double x) {
        return tCalc.ctg(x);
    }
}
