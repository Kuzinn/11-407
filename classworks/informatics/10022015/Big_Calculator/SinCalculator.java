package SecondSemestrITIS.first_lesson.Big_Calculator;

/**
 * Created by Дмитрий on 07.02.2015.
 */
public interface SinCalculator {
    public double sin(double x);
    public double cos(double x);
    public double tg (double x);
    public double ctg(double x);
}
