package DataAlgorithmsAndStructure.thirdlesson;

/**
 * Created by Дмитрий on 28.02.2015.
 */
public class MyList implements List {
    private int[] array;
    private int lastindex = 0;
    private int n = 10;

    public MyList() {
        array = new int[10];
    }

    @Override
    public void add(int value) throws IndexOutOfBoundsException {
        if (array.length==lastindex) {
            n*=2;
            int[] newarray = new int[n];
            System.arraycopy(array, 0, newarray, 0, array.length);
            array = newarray;
        }
        array[lastindex++] = value;
    }

    @Override
    public void add(int index, int value) throws IndexOutOfBoundsException  {
        if (array.length==lastindex) {
            n*=2;
            int[] newarray = new int[n];
            System.arraycopy(array, 0, newarray, 0, array.length);
            array = newarray;
        }
        int buf=index;
        System.arraycopy(array,index--,array,index+=2,lastindex-index+1);
        array[buf]=value;
        lastindex++;
    }

    @Override
    public void remove(int index) {
        System.arraycopy(array, index, array, index - 1, array.length - index);
    }

    @Override
    public int get(int index) throws IndexOutOfBoundsException {
        int a = 0;

        for (int i = 0; i < array.length; i++) {
            a = array[index];
        }

        return a;
    }

    @Override
    public int size() {
        return lastindex;
    }

    @Override
    public void addRange(List list) {

    }
}
