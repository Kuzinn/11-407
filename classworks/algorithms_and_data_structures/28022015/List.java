package DataAlgorithmsAndStructure.thirdlesson;

/**
 * Created by Дмитрий on 28.02.2015.
 */
public interface List {
    public void add(int value);
    public void add(int index,int value);
    public void remove(int index);
    public int get(int index);
    public int size();
    public void addRange(List list);
}
