package DataAlgorithmsAndStructure.secondlesson;

/**
 * Created by Дмитрий on 21.02.2015.
 */
public class MyStack implements Stack {
    private int[] array;
    private int lastindex=0;
    private int n=10;
    public MyStack(){

        array=new int[n];
    }

    @Override
    public void push(int value) {
        if (size()==lastindex){
            int[] newarray=new int[2*n];
            System.arraycopy(array,0,newarray,0,array.length);
            array=newarray;
        }
        array[lastindex++]=value;
    }

    @Override
    public int pop() {
        int buf=lastindex;
        array[lastindex]=0;
        lastindex--;
        return array[buf];
    }

    @Override
    public int size() {
        return array.length;
    }
    public int[] givearray(){
        return array;
    }
}
