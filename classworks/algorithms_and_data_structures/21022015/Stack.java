package DataAlgorithmsAndStructure.secondlesson;

/**
 * Created by Дмитрий on 21.02.2015.
 */
public interface Stack {
    public void push(int value);
    public int pop();
    public int size();
}
